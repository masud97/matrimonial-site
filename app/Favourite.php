<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Favourite extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'favourites';

    public function candidate_user(){
        return $this->belongsTo(User::class, 'candidate_id');
    }

    public function favourite_user(){
        return $this->belongsTo(User::class, 'favourite_id');
    }

}
