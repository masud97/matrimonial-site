<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Choice;

class MaritalStatus extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'marital_statuses';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function choices(){
        return $this->hasMany(Choice::class);
    }
}
