<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\ForWhome;
use App\ChatRequest;
use App\Chat;
use App\Favourite;

class User extends Authenticatable
{
    use Notifiable;

    const VERIFIED_USER = 1;
    const UNVERIFIED_USER = 0;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'mobile',
        'password', 
        'image', 
        'for_whome_id', 
        'gender', 
        'verification_token', 
        'verified',
        'created_at',
        'updated_at',
        'deleted_at',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $table = 'users';

    public function isVerified(){
        return $this->verified == User::VERIFIED_USER;
    }

    public static function generateVerificationCode(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 6; $i++) {
            $randomString .= $characters[rand(0,25)];
        }
        return $randomString;
    }

    public static function scopeVerified($query){
        return $query->where('verified', 1);
    }

    public static function scopeCompleteprofile($query){
        return $query->where('profile_creation_stage', 0);
    }
    
    public function for_whome(){
        return $this->belongsTo(ForWhome::class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function choice(){
        return $this->hasOne(Choice::class);
    }  

    public function chat_requests(){
        return $this->hasMany(ChatRequest::class);
    }

    public function chats(){
        return $this->hasMany(Chats::class);
    }

    public function favourites(){
        return $this->hasMany(Favourite::class);
    }

}
