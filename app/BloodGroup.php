<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;

class BloodGroup extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'blood_groups';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }
}
