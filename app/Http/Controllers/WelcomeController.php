<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ForWhome;
use App\Religion;
use App\Division;
use App\Profile;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;
use App\Caste;
use App\District;
use App\Education;



class WelcomeController extends Controller
{
    public function check_auth(){        
            $forWhomeList = ForWhome::all();
            $religions = Religion::all();
            $divisions = Division::all();
            return view('welcome', ['divisions' => $divisions, 'forWhomeList' => $forWhomeList, 'religions' => $religions]);
    }


    public function visitor_search(Request $request){

        $gender = $request->input('gender');
        $ageFrom = age2dob($request->input('age-from'));
        $ageTo = age2dob($request->input('age-to'));
        $religion = $request->input('religion');
        $division = $request->input('division');


        $visitorSearch = Profile::where('religion_id', $religion)
            ->whereHas('user', function($q) use($gender){
                $q->where('gender', '!=', $gender);
            })
            ->where('division_id', $division)            
            ->whereBetween('dob',[$ageTo,$ageFrom])
            ->with('religion','occupation', 'employe_type', 'income_range')
            ->get();  
/*
        echo "<pre>";
        print_r($visitorSearch);
        die();
        
*/
        return view('pages.visitor.index',['visitorSearch' => $visitorSearch]);
    }

    public function visitor_showdetails($id){
        $details = Profile::where('id', $id)->with('religion', 'caste', 'occupation', 'employe_type', 'income_range', 'present_district', 'education')->first();
        return response()->json(['details' => $details], 200);
    }
}
