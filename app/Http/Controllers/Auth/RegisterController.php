<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::check() && Auth::user()->profile_creation_stage == 0){            
            $this->redirectTo = route('user.dashboard');
        }elseif(Auth::check() && Auth::user()->profile_creation_stage != 0){
            $this->redirectTo = route('user.profile.create');
        }else{
            $this->middleware('guest');
        }        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'mobile' => ['required', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $email = $data['email'];
        $mobile = $data['mobile'];
        $token = User::generateVerificationCode();
        $message = 'Your Account Verification code is: '.$token;
        $forWhomeId = $data['for_whome'];         

        $mail = array(
            'email' => $email,
            'name' => 'Matrimonial',
            'subject' => 'Account verification code from Matrimonial site',
            'body' => $token
        );
        sendEmail($mail);
        // sendSms($mobile, $message);
        /*
        $array = array(
            'email' => $email,
            'mobile' => $mobile,
            'for_whome_id' => $forWhomeId,
            'gender' => $data['gender'],
            'password' => Hash::make($data['password']),
            'created_at' => date('Y-m-d'),
        );
        /*
        echo "<pre>";
        print_r($array);
        die();
        */
        return User::create([
            'email' => $email,
            'mobile' => $mobile,
            'verification_token' => $token,
            'for_whome_id' => $forWhomeId,
            'gender' => $data['gender'],
            'password' => Hash::make($data['password']),
            'created_at' => date('Y-m-d'),
        ]);
    }
}
