<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\ChatRequest;
use App\User;
use App\Profile;

class ChatRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $userId = Auth::id();
        $receiverProfileId = (int)$id;

        $receiverId = Profile::select('user_id', 'full_name')->where('id', $id)->first();
    
        $checkRequest = ChatRequest::where('sender_user_id',$userId)
        ->where('receiver_user_id', $receiverId->user_id)
        ->orWhere('receiver_user_id', $userId)
        ->where('sender_user_id', $receiverId->user_id)
        ->first();

        if(is_null($checkRequest)){            
            $chatRequest = new ChatRequest();
            $chatRequest->sender_user_id = $userId;
            $chatRequest->receiver_user_id = $receiverId->user_id;
            $chatRequest->created_at = date('Y-m-d');
            $chatRequest->save();
            addTrail('User has successfully send the chat request to '.$id.'.', 'Success');
            Toastr::success('Your chat request has sent to '.$receiverId->full_name);
            return response()->json(['message' => 'Your chat request has sent to '.$receiverId->full_name], 200);
        }else{                
            if($checkRequest->status == 1){
                Toastr::error('You have already connected with '.$receiverId->full_name);
                return response()->json(['message' => 'You have already connected with '.$receiverId->full_name], 200);
            }else{
                Toastr::error('You have already send the chat request to '.$receiverId->full_name);
                return response()->json(['message' => 'You have already send the chat request to '.$receiverId->full_name], 200);
            }           
            
        }        
    }

    public function notifications(){
        $userId = Auth::id();

        $notifications =  ChatRequest::where('receiver_user_id', $userId)->where('status', 0)->count();

        return response()->json(['notifications' => $notifications], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notification_details()
    {
        $userId = Auth::id();
        $notifications =  ChatRequest::where('receiver_user_id', $userId)->where('status', 0)->with('sender_user')->paginate(12);
        
        

        return view('pages.chat.request', ['notifications' => $notifications]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function accept_request($id)
    {
        $userId = Auth::id();
        $senderId = Profile::select('user_id', 'full_name')->where('id',$id)->first();

        $update = ChatRequest::where('receiver_user_id', $userId)->where('sender_user_id', $senderId->user_id)->update(['status' => 1]);
        Toastr::success('You have accepted the chat request of '.$senderId->full_name. '. Now you can chat with '.$senderId->full_name);
        return response()->json(['id' => $senderId], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function denied_request($id)
    {
        $userId = Auth::id();
        $senderId = Profile::select('user_id', 'full_name')->where('id',$id)->first();

        $deleteResult = ChatRequest::where('receiver_user_id', $userId)->where('sender_user_id', $senderId->user_id)->delete();

        if($deleteResult){
            Toastr::success('You have denied the chat request of '.$senderId->full_name);
            return response()->json(['success' => 'Ok'], 200);
        }else{
            Toastr::error('Woops!! Something went wrong, try again');
            return response()->json(['success' => 'Ok'], 200);
        }
    }
}
