<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Brian2694\Toastr\Facades\Toastr;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Choice;
use App\Profile;
use App\Country;
use App\District;
use App\Division;
use App\Religion;
use App\Caste;
use App\MaritalStatus;
use App\BloodGroup;
use App\Immigration;
use App\NRB;
use App\Education;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;
use App\Employment;
use DB;
use Carbon\Carbon;
use File;

class UserController extends Controller
{
    public function create_profile(){
        $stage = User::select('profile_creation_stage')->where('id', Auth::user()->id)->first();

        if($stage->profile_creation_stage == 1){
            $countries = Country::all();
            $districts = District::all();
            $religions = Religion::all();
            $castes = Caste::all();
            $maritalStatus = MaritalStatus::all();
            $bloodGroups = BloodGroup::all();
            $immigrations = Immigration::all();

            $educations = Education::all();
            $employeTypes = EmployeType::all();
            $occupations = Occupation::all();
            $imcomeRanges = IncomeRange::all();

            return view('pages.users.profile.profile-create-'.$stage->profile_creation_stage,[
                'countries' => $countries,
                'districts' => $districts,
                'religions' => $religions,
                'castes' => $castes,
                'maritalStatus' => $maritalStatus,
                'bloodGroups' => $bloodGroups,
                'immigrations' => $immigrations,
                'educations' => $educations,
                'employeTypes' => $employeTypes,
                'occupations' => $occupations,
                'imcomeRanges' => $imcomeRanges

            ]);
        }elseif($stage->profile_creation_stage == 2){
            $educations = Education::all();
            $employeTypes = EmployeType::all();
            $occupations = Occupation::all();
            $imcomeRanges = IncomeRange::all();
            return view('pages.users.profile.profile-create-'.$stage->profile_creation_stage,[
                'educations' => $educations,
                'employeTypes' => $employeTypes,
                'occupations' => $occupations,
                'imcomeRanges' => $imcomeRanges
            ]);
        }else{
            return view('pages.users.profile.profile-create-'.$stage->profile_creation_stage);
        }

        
    }

    public function religion_casste($id){
        $castes = Caste::where('religion_id', $id)->get();
        return response()->json(['castes' => $castes], 200);
    }



    public function store_profile_first(Request $request){

        $this->validate($request, [
            'f_name' => 'required',
            'dob' => 'required',
            'religion' => 'required',
            'marital_status' => 'required',
            'height_in_feet' => 'required',
            'nrb-country' => 'required',
            'home-district' => 'required',
            'education' => 'required',
            'employmenttype' => 'required',
            'code' => 'required|max:6'
        ],[
            'f_name.required' => 'Full name is required.',
            'dob.required' => 'Date of birth is required.',
            'religion.required' => 'Religion is required.',
            'marital_status.required' => 'Marital status is required.',
            'height_in_feet.required' => 'Height is required.',
            'nrb-country.required' => 'Country is required.',
            'home-district.required' => 'Home district is required',
            'employmenttype.required' => 'Employment type is required',
            'code.required' => 'Your user verification code must not be empty',
            'code.max' => 'User verification code length must be 6 digits long'
        ]); 

        $userId = Auth::user()->id;

        if(!empty($request->input('code'))){
            $user = User::select('id', 'verification_token')->where('id', $userId)->first();
            $code = $request->input('code');
            $verification = user_verify($user->verification_token, $code);

            if($verification){
                $updateVerifiedStatus = User::where('id', $userId)->update(['updated_at' => date('Y-m-d'), 'verified' => 1]);
                
                $heightFeet = $request->input('height_in_feet');
                $heightInches = $request->input('height_in_inches');
                $height = feet2inches($heightFeet, $heightInches);
                $newCaste = $request->input('caste-text');
                $state = $request->input('state');
                $immigrationType = $request->input('immigration-type');
                $homeDistrictId = $request->input('home-district');
                $divisionId  = District::select('division_id')->where('id',$homeDistrictId)->first();
                
                

                

                $profile = new Profile;
                $profile->user_id = $userId;
                $profile->full_name = $request->input('f_name');
                //$profile->name_status = $request->get('namestatus');
                $profile->dob = changeDateFormat($request->input('dob'));
                $profile->height = $height;
                $profile->weight = $request->input('weight');
                $profile->blood_group_id = $request->input('blood_group');
                $profile->religion_id = $request->input('religion');
                if(!isset($newCaste)){
                    $profile->caste_id = $request->input('caste');
                }else{            
                    $caste = new Caste();
                    $caste->religion_id = (int)$request->get('religion');
                    $caste->english_name = $newCaste;
                    $caste->created_at = date('Y-m-d');
                    $caste->save();
                    $profile->caste_id = $caste->id;            
                }
                $profile->marital_status_id = $request->input('marital_status');
                $profile->country_id = $request->input('nrb-country');

                if(!empty($request->input('residence-address'))){
                    $profile->present_address = $request->input('residence-address');
                }

                if(!empty($request->input('residency-type'))){
                    $profile->residence_type = $request->input('residency-type');
                }

                if(!empty($request->input('present_district'))){
                    $profile->present_district_id = $request->input('present_district');
                }
                    

                if(isset($homeDistrictId)){
                    $profile->home_district_id = $homeDistrictId;
                    $profile->division_id = $divisionId->division_id;
                }

                $profile->education_id = $request->input('education');
                $profile->edu_details = $request->input('edu_detail');
                $profile->institution = $request->input('institute');
                $profile->employe_type_id = $request->input('employmenttype');
                $profile->occupation_id = $request->input('occupation');
                $profile->income_range_id = $request->input('income');
                $profile->personal_details = $request->input('personal-details');

                // Save profile image
                if($request->has('image')){
                    $image = $request->file('image'); 
                    $slug = str_slug($request->input('f_name'));
                    $imageName = $slug.'_'.uniqid().'.'.$image->getClientOriginalExtension();
                    $request->image->move(('assets/img/profile'), $imageName);  
                }

                // Save Id proof image                                                                                                                        
                if($request->has('idproof')){
                    $image = $request->file('idproof'); 
                    $slug = str_slug($request->input('f_name'));
                    $imageName = $slug.'_'.uniqid().'.'.$image->getClientOriginalExtension();
                    $request->idproof->move(('assets/img/idproof'), $imageName);  
                }       
                //Save profile...
                $finalProfileCreate = $profile->save();  


                if($profile->country_id != 18){
                    $nrb = new NRB();
                    $nrb->profile_id = $profile->id;
                    $nrb->country_id = (int)$request->get('country');
                    if(isset($state)){
                        $nrb->state = $state;
                    }    
                    if(isset($immigrationType)){
                        $nrb->immigration_type_id = $immigrationType;
                    }
                    $nrb->created_at = date('Y-m-d');
                    $nrb->save();
                } 

                
                if(!empty($request->input('employer'))){
                    $employer = new Employment();
                    $employer->profile_id = $profile->id;
                    $employer->employer = $request->input('employer');
                    $employer->employer_address = $request->input('emp-address');
                    $employer->position = $request->input('position');

                    $employer->created_at = date('Y-m-d');
                    $employer->save();
                }
                if($finalProfileCreate){
                    $updateProfileCreationStage = User::where('id', $userId)->update(['updated_at' => date('Y-m-d'), 'profile_creation_stage' => 0]);
                    Toastr::success('You have successfully created your profile', 'success');
                    return redirect()->route('user.dashboard');
                }else{
                    Toastr::warning('W00ps! Something went wrong. Profile creation failed', 'warning');
                    return redirect()->back();
                }


            }else{                
                Toastr::warning('W00ps! Something went wrong. Invalid user verification code.', 'warning');
                return redirect()->back();
            }     
        }else{        
            Toastr::warning('W00ps! Something went wrong. No user verification code found.', 'warning');
            return redirect()->back();      
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "after login i am here";
        //die();
        $id = Auth::user()->id;

        $ifSetChoice = Choice::where('user_id', $id)->first();        
         

        if($ifSetChoice){            
            $user = User::where('id', $id)->with('choice', 'profile')->first(); 
            $ageFrom = age2dob($user->choice->age_from);
            $ageTo = age2dob($user->choice->age_to);  
            $heightFrom = $user->choice->height_from;
            $heightTo = $user->choice->height_to;     
            $myDivision = $user->profile->division;
            $religion = $user->profile->religion;        
            $gender = $user->gender;              
            
            $selectedCandidates = Profile::where('religion_id', $religion->id)
                ->whereHas('user', function($q) use($gender){
                    $q->where('gender', '!=', $gender);
                })
                ->where('division_id', $myDivision->id)
                ->whereBetween('dob',[$ageTo, $ageFrom])
                ->whereBetween('height',[$heightFrom, $heightTo])
                ->with('religion','occupation', 'employe_type', 'income_range')
                ->paginate(12);  
            return view('pages.users.dashboard',['selectedCandidates' => $selectedCandidates]);              
        }else{
            $user = User::where('id', $id)->with('profile')->first();
            
            $myDivision = $user->profile->division;
            $religion = $user->profile->religion;        
            $gender = $user->gender;

            $selectedCandidates = Profile::where('religion_id', $religion->id)
                ->whereHas('user', function($q) use($gender){
                    $q->where('gender', '!=', $gender);
                })
                ->where('division_id', $myDivision->id)   
                ->with('religion','occupation', 'employe_type', 'income_range')
                ->paginate(12);
            return view('pages.users.dashboard',['selectedCandidates' => $selectedCandidates]);
        }   
        
        
    }

    public function candidate_details($id){
        $details = Profile::where('id', $id)->with('religion', 'caste', 'occupation', 'employe_type', 'income_range', 'present_district', 'education')->first();
        return response()->json(['details' => $details], 200);
    }


    public function resend_code()
    {        
        
        $id = Auth::user()->id;
        $user = User::select('id', 'email', 'mobile', 'verification_token')->where('id', $id)->first();

        $token = User::generateVerificationCode();

        if($token){
            $updateVerifiedToken = User::where('id', $id)->update(['verification_token' => $token]);
            if($updateVerifiedToken){
                $email = $user->email;
                $mobile = $user->mobile;                
                $message = 'Your Account Verification code is: '.$token;

                $mail = array(
                    'email' => $email,
                    'name' => 'Matrimonial',
                    'subject' => 'Account verification code from Matrimonial site',
                    'body' => $token
                );

                sendEmail($mail);
                // sendSms($mobile, $message);                
                return response()->json(['message' => 'New verification code has sent to your email.'], 200);
            }else{                
                return response()->json(['message' => 'Something went wrong, try again!'], 200);
            }            
        }else{            
            return response()->json(['message' => 'Something went wrong, try again!'], 200);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
