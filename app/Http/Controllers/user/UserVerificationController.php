<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use Carbon\Carbon;

class UserVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.users.verification.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_verification(Request $request)
    {  

        $this->validate($request, [
            'code' => 'required|max:6'
        ],[
            'name.required' => 'verification Code is required.',
            'email.max' => 'Invalid Verification Code length.'
        ]);

        $id = Auth::user()->id;
        $user = User::select('id', 'verification_token', 'token_generation_time')->where('id', $id)->first();


        $diff = Carbon::now('+6:00')->diffInSeconds($user->token_generation_time);
        

        if($diff <= 1800){ // Check validation time for 30 minutes
            $code = $request->input('code');
            $verification = user_verify($user->verification_token, $code);
            if($verification){ // Check verification code is valid or not
                $updateVerifiedStatus = User::where('id', $id)->update(['updated_at' => date('Y-m-d'), 'verified' => 1]);
                if($updateVerifiedStatus){ // Check varified status update
                    return redirect()->route('user.dashboard');
                }else{
                    // Message should be send to use that status not updated
                    addTrail('User tried to verified account but failed.', 'Errors');
                    Toastr::warning('Woops! something went wrong, try again.', 'warning');
                    return redirect()->back();
                }
            }else{
                // Message should be send to use that code are not match
                addTrail('User tried to verified account but code not match.', 'Errors');
                Toastr::warning('Incorrect verification code.', 'warning');
                return redirect()->back();
            }
        }else{
            // Message should be send user that code has expired
            addTrail('User tried to verified account but code existing time expired.', 'Errors');
            Toastr::warning('Verification code existing time out, resend the code.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend_code()
    {
        $id = Auth::user()->id;
        $user = User::select('id', 'email', 'mobile', 'verification_token','resend_token_count')->where('id', $id)->first();


        if($user->resend_token_count > 0){ // Check the number of resend time
            $token = User::generateVerificationCode();
            
            if($token){
                $updateVerifiedToken = User::where('id', $id)->update(['verification_token' => $token, 'token_generation_time' => Carbon::now('+6:00')]);
                if($updateVerifiedToken){
                    $email = $user->email;
                    $mobile = $user->mobile;                
                    $message = 'Your Account Verification code is: '.$token;

                    $mail = array(
                        'email' => $email,
                        'name' => 'Matrimonial',
                        'subject' => 'Account verification code from Matrimonial site',
                        'body' => $token
                    );

                    sendEmail($mail);
                    // sendSms($mobile, $message);
                    $user->decrement('resend_token_count');

                    return redirect()->back();
                }else{
                    return redirect()->back();
                }
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }       
    }
}
