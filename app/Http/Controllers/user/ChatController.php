<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use App\ChatRequest;
use App\Chat;
use App\Profile;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        // User::where('id', $id)->update(['login_status' => 1]);

        $users = ChatRequest::where('sender_user_id',$id)
        ->where('status', 1) 
        ->orWhere('receiver_user_id', $id)
        ->where('status', 1)
        ->with('sender_user', 'receiver_user')          
        ->get(); 

        /*
        echo "<pre>";
        print_r($users);
        die();
        */

        return view('pages.chat.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function message($id)
    {
        // echo $id;
        $userId = Auth::id();
        $permission = ChatRequest::where('sender_user_id',$id)
        ->where('receiver_user_id', $userId)
        ->where('status', 1) 
        ->orWhere('receiver_user_id', $id)
        ->where('sender_user_id', $userId)
        ->where('status', 1)
        ->first();

        if($permission){
            $messages = Chat::where('sender_user_id',$id)
            ->where('receiver_user_id', $userId)
            ->orWhere('receiver_user_id', $id)
            ->where('sender_user_id', $userId)
            ->with('sender_user', 'receiver_user')             
            ->get();
            return response()->json(['messages' => $messages], 200);
            
        }else{
            echo "Sorry no data found";
        }
        
    }


    public function view_name($id){
        $userName = Profile::select('full_name', 'image')->where('user_id', $id)->first();
        return response()->json(['userName' => $userName], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send_message(Request $request)
    {
        $userId = Auth::id();
        $receiverId = $request->id;
        $message = $request->message;

        $permission = ChatRequest::where('sender_user_id',$userId)
        ->where('receiver_user_id', $receiverId)
        ->where('status', 1) 
        ->orWhere('receiver_user_id', $userId)
        ->where('sender_user_id', $receiverId)
        ->where('status', 1)
        ->first();

        if($permission){
            $chat = new Chat();
            $chat->sender_user_id = $userId;
            $chat->receiver_user_id = $receiverId;
            $chat->message = $message;
            $chat->created_at = date('Y-m-d');
            $chat->save();
        }
        return response()->json(['message' => $permission], 200); 
        
    }


    public function unread_message(){
        $userId = Auth::id();
        $messages = Chat::where('receiver_user_id',$userId)
            ->where('is_seen', 1) 
            ->distinct('sender_user_id')         
            ->count();
        return response()->json(['messages' => $messages], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteResult = Chat::find($id)->delete();
        if($deleteResult){
            Toastr::success('Conversation has deleted.', 'success');
            return response()->json(['message' => 'Ok'], 200);
        }
        
    }
}
