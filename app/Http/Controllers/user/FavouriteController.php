<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\Favourite;
use App\User;
use App\Profile;

class FavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $userId = Auth::id();
        $receiverProfileId = (int)$id;

        $receiverId = Profile::select('user_id', 'full_name')->where('id', $id)->first();

        $favouriteId = Favourite::where('candidate_id',$userId)
        ->where('favourite_id', $receiverId->user_id)        
        ->first();

        if(is_null($favouriteId)){            
            $newFavouriteList = new Favourite();
            $newFavouriteList->candidate_id = $userId;
            $newFavouriteList->favourite_id = $receiverId->user_id;
            $newFavouriteList->created_at = date('Y-m-d');
            $newFavouriteList->save();
            addTrail('User has successfully add favourite id', 'Success');
            Toastr::success('Successfully add to your favourite list.');
            return response()->json(['message' => 'Successfully add to your favourite list.'], 200);
        }else{   
            Toastr::error('This candidate has already added in your list.');
            return response()->json(['message' => 'This candidate has already added in your list.'], 200);
            }            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();        

        $users = Favourite::where('candidate_id',$id)->with('candidate_user','favourite_user')->paginate(12);
        
        // echo "<pre>";
        // print_r($users);
        // die();

        return view('pages.favourite.index', ['users' => $users]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = Auth::id();
        $senderId = Profile::select('user_id')->where('id',$id)->first();

        $deleteResult = Favourite::where('candidate_id', $userId)->where('favourite_id', $senderId->user_id)->delete();

        if($deleteResult){
            Toastr::success('Successfully deleted');
            return response()->json(['success' => 'Ok'], 200);
        }else{
            Toastr::error('Woops!! Something went wrong, try again');
            return response()->json(['success' => 'Ok'], 200);
        }
    }
}
