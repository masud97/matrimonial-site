<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use App\Choice;
use App\Division;
use App\Profile;
use App\District;
use App\Religion;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;
use DB;

class CandidateProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $id = Auth::user()->id;
        
        $user = User::where('id', $id)->with('choice', 'profile')->first();

        $choiceDivision = $user->choice->division;
        $religion = $user->profile->religion;
        $choiceHeightFrom = $user->choice->height_from;
        $choiceHeightTo = $user->choice->height_to;
        $choiceAgeFrom = age2dob($user->choice->age_from);
        $choiceAgeTo = age2dob($user->choice->age_to);
        $gender = $user->gender;

        /*
        echo $choiceDivision."<br/>".$religion."<br/>".$choiceHeightFrom."<br/>".$choiceHeightTo. 
        "<br/>".$choiceAgeFrom."<br/>".$choiceAgeTo."<br/>".$gender;
        */
        $selectedCandidates = Profile::where('religion_id', $religion->id)
            ->whereHas('user', function($q) use($gender){
                $q->where('gender', '!=', $gender);
            })
            ->where('division_id', $choiceDivision->id)
            ->whereBetween('height',[$choiceHeightFrom,$choiceHeightTo])
            ->whereBetween('dob',[$choiceAgeTo,$choiceAgeFrom])
            ->with('religion','occupation', 'employe_type', 'income_range')
            ->get();
        /*
        echo "<pre>";
        print_r($selectedCandidates);
        die();
        */
        return view('pages.users.candidate.index',['selectedCandidates' => $selectedCandidates]);
    }



    public function details($id){
        $candidate = Profile::where('id', $id)
        ->with('religion','occupation', 'employe_type', 'income_range')
        ->first();

        return view('pages.users.candidate.show',['candidate' => $candidate]);
    }
}
