<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Brian2694\Toastr\Facades\Toastr;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Choice;
use App\Profile;
use App\Country;
use App\District;
use App\Division;
use App\Religion;
use App\Caste;
use App\MaritalStatus;
use App\BloodGroup;
use App\Immigration;
use App\NRB;
use App\Education;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;
use App\Employment;
use DB;
use Carbon\Carbon;
use File;

class UserProfileUpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $profileId = Profile::select('id')->where('user_id', $userId)->first();
        $profileDetails = Profile::where('id', $profileId->id)
        ->with('country', 'present_district', 'home_district','religion', 'caste', 'occupation', 'employe_type', 'income_range', 'marital_status', 'nrb', 'employment', 'education')
        ->first();
        return view('pages.users.profile.show',['profileDetails' => $profileDetails]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
