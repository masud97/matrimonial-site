<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use App\Profile;
use App\Choice;
use App\MaritalStatus;
use App\Education;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;
use App\Employment;
use App\Division;

class ChoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::user()->id;
        $choice = Choice::where('user_id', $id)->first();
        if(isset($choice) && !is_null($choice)){
            if($id == $choice->user_id){
                addTrail('User has successfully show his/her choice.', 'Success');
                return view('pages.users.choice.show',['choice' => $choice]);
            }else{
                addTrail('User has tried to access unauthorized choice.', 'Errors');
                Toastr::warning('Access Denied.', 'warning');
                return redirect()->back();
            }
        }else{
            $maritalStatus = MaritalStatus::all();
            $divisions = Division::all();
            $educations = Education::all();
            $employeTypes = EmployeType::all();
            $occupations = Occupation::all();
            $imcomeRanges = IncomeRange::all();
            return view('pages.users.choice.create',[
                'educations' => $educations,
                'employeTypes' => $employeTypes,
                'occupations' => $occupations,
                'imcomeRanges' => $imcomeRanges,
                'maritalStatus' => $maritalStatus,
                'divisions' => $divisions
            ]);
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'age-from' => 'required',
            'age-to' => 'required',
            'feet-from' => 'required',
            'feet-to' => 'required',
            'marital-status' => 'required',
            'division' => 'required',
            'education' => 'required',
        ],[
            'age-from.required' => 'Select from age.',
            'age-to.required' => 'Select to age.',
            'feet-from.required' => 'Select feet from.',
            'feet-to.required' => 'Select feet to.',
            'marital-status.required' => 'Select marital status.',
            'division.required' => 'Select division.',
            'education.required' => 'Select education.'
        ]);        

        $choice = new Choice;
        $choice->user_id = Auth::user()->id; 
        if($request->has('age-from')){
            $choice->age_from = $request->input('age-from');
        }

        if($request->has('age-to')){
            $choice->age_to = $request->input('age-to');
        }
        
        if($request->has('feet-from')){
            $choice->height_from = feet2inches($request->input('feet-from'), 0);
        }
        if($request->has('feet-to')){
            $choice->height_to = feet2inches($request->input('feet-to'), 0);
        }
        if($request->has('marital-status')){
            $choice->marital_status_id = $request->input('marital-status');
        }
        if($request->has('division')){
            $choice->division_id = $request->input('division');
        }
        if($request->has('education')){
            $choice->education_id = $request->input('education');
        }
        if($request->has('employe-type')){
            $choice->employe_type_id = $request->input('employe-type');
        }
        if($request->has('occupation')){
            $choice->occupation_id = $request->input('occupation');
        }
        if($request->has('income-range')){
            $choice->income_range_id = $request->input('income-range');
        } 
        
        $choice->created_at = date('Y-m-d');

        if ($choice->save()) {            
            addTrail('User has successfully set his/her choice.', 'Success');
            Toastr::success('Your choice has saved now.', 'success');
            return redirect()->route('user.choice.show',['id' => $choice->id]);
        }else{
            addTrail('User has failed to set his/her choice.', 'Errors');
            Toastr::warning('Your choice is not saved.', 'warning');
            return redirect()->back();
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userId = Auth::user()->id;

        $choice = Choice::where('id', $id)->first();
        if($userId == $choice->user_id){
            addTrail('User has successfully show his/her choice.', 'Success');
            return view('pages.users.choice.show',['choice' => $choice]);
        }else{
            addTrail('User has tried to access unauthorized choice.', 'Errors');
            Toastr::warning('Access Denied.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userId = Auth::user()->id;
        $choice = Choice::where('id', $id)->first();
        if($userId == $choice->user_id){            
            $maritalStatus = MaritalStatus::all();
            $divisions = Division::all();
            $educations = Education::all();
            $employeTypes = EmployeType::all();
            $occupations = Occupation::all();
            $imcomeRanges = IncomeRange::all();

            return view('pages.users.choice.edit',[
                'choice' => $choice,
                'educations' => $educations,
                'employeTypes' => $employeTypes,
                'occupations' => $occupations,
                'imcomeRanges' => $imcomeRanges,
                'maritalStatus' => $maritalStatus,
                'divisions' => $divisions
            ]);
        }else{
            addTrail('User has tried to edit unauthorized choice.', 'Errors');
            Toastr::warning('Access Denied.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;
        $choice = Choice::where('id', $id)->first();
        if($userId == $choice->user_id){
            $choice = array(
                'user_id' => Auth::user()->id,
                'age_from' => $request->input('age-from'),
                'age_to' => $request->input('age-to'),
                'height_from' => feet2inches($request->input('feet-from'), $request->input('inche-from')),
                'height_to' => feet2inches($request->input('feet-to'), $request->input('inche-to')),
                'marital_status_id' => $request->input('marital-status'),
                'division_id' => $request->input('division'),
                'education_id' => $request->input('education'),
                'employe_type_id' => $request->input('employe-type'),
                'occupation_id' => $request->input('occupation'),
                'income_range_id' => $request->input('income-range'),
                'updated_at' => date('Y-m-d')
            );

            $update = Choice::where('id', $id)->update($choice);
            if($update){
                addTrail('User has successfully update his/her choice.', 'Success');
                Toastr::success('Your choice has updated now.', 'success');
                return redirect()->route('user.choice.show',['id' => $id]);
            }else{
                addTrail('User has failed to update his/her choice.', 'Errors');
                Toastr::warning('Woops! failed to update.', 'warning');
                return redirect()->back();
            }
        }else{
            addTrail('User has tried to edit unauthorized choice.', 'Errors');
            Toastr::warning('Access Denied.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
