<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Choice;
use App\Profile;
use Brian2694\Toastr\Facades\Toastr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->profile_creation_stage == 0 ) {
            addTrail('User has been logged in successfully.', 'Success');
            Toastr::success('Login successfull.', 'success');
            return redirect()->route('user.dashboard');
        }else{
            addTrail('User has been logged in successfully with incomplete profile.', 'Success');
            Toastr::warning('Your profile is not completed.', 'warning');            
            return redirect()->route('user.profile.create');
        }
    }

    public function show_demo(){

        $id = Auth::user()->id;

        $ifSetChoice = Choice::where('user_id', $id)->first();        
         

        if($ifSetChoice){            
            $user = User::where('id', $id)->with('choice', 'profile')->first(); 
            $ageFrom = age2dob($user->choice->age_from);
            $ageTo = age2dob($user->choice->age_to);  
            $heightFrom = $user->choice->height_from;
            $heightTo = $user->choice->height_to;     
            $myDivision = $user->profile->division;
            $religion = $user->profile->religion;        
            $gender = $user->gender;              
            
            $selectedCandidates = Profile::where('religion_id', $religion->id)
                ->whereHas('user', function($q) use($gender){
                    $q->where('gender', '!=', $gender);
                })
                ->where('division_id', $myDivision->id)
                ->whereBetween('dob',[$ageTo, $ageFrom])
                ->whereBetween('height',[$heightFrom, $heightTo])
                ->with('religion','occupation', 'employe_type', 'income_range')
                ->paginate(12);                
        }else{
            $user = User::where('id', $id)->with('profile')->first();
            
            $myDivision = $user->profile->division;
            $religion = $user->profile->religion;        
            $gender = $user->gender;

            $selectedCandidates = Profile::where('religion_id', $religion->id)
                ->whereHas('user', function($q) use($gender){
                    $q->where('gender', '!=', $gender);
                })
                ->where('division_id', $myDivision->id)   
                ->with('religion','occupation', 'employe_type', 'income_range')
                ->paginate(12);
        }        
        
        //return view('pages.users.dashboard',['selectedCandidates' => $selectedCandidates]);

        return view('pages.demo',['selectedCandidates' => $selectedCandidates]);
    }
}
