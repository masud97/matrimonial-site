<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Choice;

class EmployeType extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'employe_types';
    
    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function choices(){
        return $this->hasMany(Choice::class);
    }
}
