<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Immigration;

class NRB extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'n_r_bs';

    public function profile(){
        return $this->belongsTo(Profile::class);
    }

    public function immigration(){
        return $this->belongsTo(Immigration::class);
    }
}
