<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\District;
use App\Choice;
use App\Profile;

class Division extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'divisions';

    public function districts(){
        return $this->hasMany(District::class);
    }

    public function choices(){
        return $this->hasMany(Choice::class);
    }

    public function profiles(){
        return $this->hasMany(Profile::class);
    }
}
