<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Religion;

class Caste extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'castes';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function religion(){
        return $this->belongsTo(Religion::class);
    }
}
