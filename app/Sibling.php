<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;

class Sibling extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'siblings';

    public function profile(){
        return $this->belongsTo(Sibling::class);
    }
}
