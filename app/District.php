<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Division;
use DB;

class District extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'districts';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function division(){
        return $this->belongsTo(Division::class);
    }

    public static function divisionID ($id){
        $divisionID = DB::table('districts')->select('division_id')->where('id',$id)->first();
        return $divisionID->division_id;
    }
}
