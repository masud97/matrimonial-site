<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Country;
use App\District;
use App\Division;
use App\NRB;
use App\Employment;
use App\Sibling;
use App\EmployeType;
use App\Caste;
use App\Religion;
use App\Occupation;
use App\IncomeRange;
use App\Immigration;
use App\Education;
use App\MaritalStatus;
use App\BloodGroup;

class Profile extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'profiles';

    public static function scopeApproved($query){
        return $query->where('is_approved', 1);
    }


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function present_district(){
        return $this->belongsTo(District::class, 'present_district_id');
    }

    public function home_district(){
        return $this->belongsTo(District::class, 'home_district_id');
    }

    public function employe_type(){
        return $this->belongsTo(EmployeType::class);
    }

    public function occupation(){
        return $this->belongsTo(Occupation::class);
    }

    public function religion(){
        return $this->belongsTo(Religion::class);
    }

    public function caste(){
        return $this->belongsTo(Caste::class);
    }

    public function income_range(){
        return $this->belongsTo(IncomeRange::class);
    }

    public function marital_status(){
        return $this->belongsTo(MaritalStatus::class);
    }

    public function nrb(){
        return $this->hasOne(NRB::class);
    }

    public function employment(){
        return $this->hasOne(Employment::class);
    }    

    public function siblings(){
        return $this->hasMany(Sibling::class);
    }

    public function education(){
        return $this->belongsTo(Education::class);
    }
    
    public function blood_group(){
        return $this->belongsTo(BloodGroup::class);
    }

    public function division(){
        return $this->belongsTo(Division::class);
    }
}
