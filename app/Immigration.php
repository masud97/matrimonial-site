<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NRB;

class Immigration extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'immigrations';

    public function n_r_bs(){
        return $this->hasMany(NRB::class);
    }
}
