<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\MaritalStatus;
use App\Division;
use App\Education;
use App\EmployeType;
use App\Occupation;
use App\IncomeRange;

class Choice extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'choices';

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function marital_status(){
        return $this->belongsTo(MaritalStatus::class);
    }

    public function division(){
        return $this->belongsTo(Division::class);
    }

    public function education(){
        return $this->belongsTo(Education::class);
    }

    public function employe_type(){
        return $this->belongsTo(EmployeType::class);
    }

    public function occupation(){
        return $this->belongsTo(Occupation::class);
    }

    public function income_range(){
        return $this->belongsTo(IncomeRange::class);
    }
}
