<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ForWhome extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'for_whomes';

    public function users(){
        return $this->hasMany(User::class);
    }
}
