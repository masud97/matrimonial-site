<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Immigration;

class Country extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'countries';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function immigrations(){
        return $this->hasMany(Immigration::class);
    }
}
