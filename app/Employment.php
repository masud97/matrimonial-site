<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;

class Employment extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'employments';

    public function profile(){
        return $this->belongsTo(Profile::class);
    }
}
