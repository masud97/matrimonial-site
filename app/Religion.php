<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Profile;
use App\Caste;

class Religion extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'religions';

    public function profiles(){
        return $this->hasMany(Profile::class);
    }

    public function castes(){
        return $this->hasMany(Caste::class);
    }
}
