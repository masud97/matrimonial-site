<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChatRequest;
use App\User;

class Chat extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'chats';

    public function chat_request(){
        return $this->belongsTo(ChatRequest::class);
    }

    public function sender_user(){
        return $this->belongsTo(User::class, 'sender_user_id');
    }

    public function receiver_user(){
        return $this->belongsTo(User::class, 'receiver_user_id');
    }


}
