<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

*/

Route::get('/demo', 'HomeController@show_demo');

Route::get('/locale/{locale}', function($locale){
    
    Session::put('locale', $locale);
    return response()->json(['msg' => $locale], 200);
    //return back();
});

Route::get('/resend/code', 'user\UserController@resend_code')->name('resend.code');
Route::get('/', 'WelcomeController@check_auth');
Route::post('/visitor/search', 'WelcomeController@visitor_search')->name('visitor.search');
Route::get('/visitor/showdetails/{id}', 'WelcomeController@visitor_showdetails')->name('visitor.showdetails');

Route::get('/candidate/details/{id}', 'user\UserController@candidate_details')->name('candidate.details');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/religion/{id}', 'user\UserController@religion_casste')->name('profile.religion')->middleware('upc');

Route::group(['as'=>'user.', 'prefix'=> 'user', 'namespace' => 'user', 'middleware' => ['auth']], function(){
    Route::get('/profile/create', 'UserController@create_profile')->name('profile.create')->middleware('upc');
    Route::post('/profile/create1', 'UserController@store_profile_first')->name('profile.create1')->middleware('upc');    
    Route::post('/profile/create2', 'UserController@store_profile_second')->name('profile.create2')->middleware('upc');
    Route::post('/profile/create3', 'UserController@store_profile_third')->name('profile.create3')->middleware('upc');
    
    Route::get('/view/profile', 'UserProfileUpdateController@index')->name('view.profile')->middleware('user');

    Route::get('/dashboard', 'UserController@index')->name('dashboard')->middleware('user');
    
    Route::get('/query', 'UserController@query')->name('query')->middleware('user');
    Route::get('/choice/create', 'ChoiceController@create')->name('choice.create')->middleware('user');
    Route::post('/choice/store', 'ChoiceController@store')->name('choice.store')->middleware('user');
    Route::get('/choice/show/{id}', 'ChoiceController@show')->name('choice.show')->middleware('user');
    Route::get('/choice/edit/{id}', 'ChoiceController@edit')->name('choice.edit')->middleware('user');
    Route::post('/choice/update/{id}', 'ChoiceController@update')->name('choice.update')->middleware('user');
    Route::get('/candidate/profile', 'CandidateProfileController@index')->name('candidate.profile')->middleware('user');
    Route::get('/candidate/details/{id}', 'CandidateProfileController@details')->name('candidate.details')->middleware('user');

    Route::get('/verification', 'UserVerificationController@index')->name('verification')->middleware('verification');
    Route::post('/verified', 'UserVerificationController@user_verification')->name('verified')->middleware('verification');
    Route::get('/resend/code', 'UserVerificationController@resend_code')->name('resend.code')->middleware('verification');

    Route::get('/chat', 'ChatController@index')->name('chat')->middleware('user');
    Route::get('/message/{id}', 'ChatController@message')->name('message')->middleware('user');
    Route::get('/view/name/{id}', 'ChatController@view_name')->name('view.name')->middleware('user');
    Route::post('/send/message', 'ChatController@send_message')->name('send.message')->middleware('user');
    Route::get('/unread/message', 'ChatController@unread_message')->name('unread.message')->middleware('user');
    Route::get('/delete/message/{id}', 'ChatController@destroy')->name('delete.message')->middleware('user');

    Route::get('/send-chat-request/{id}', 'ChatRequestController@index')->name('send.chat.request')->middleware('user');

    Route::get('/notifications', 'ChatRequestController@notifications')->name('notifications')->middleware('user');
    Route::get('/notifications/view', 'ChatRequestController@notification_details')->name('notifications.view')->middleware('user');
    //Route::get('/chatrequest/denied/{id}', 'ChatRequestController@denied_request')->name('chatrequest.denied')->middleware('user');
    Route::get('/accept/request/{id}', 'ChatRequestController@accept_request')->name('accept.request')->middleware('user');
    Route::get('/denied/request/{id}', 'ChatRequestController@denied_request')->name('denied.request')->middleware('user');

    Route::get('/add-to-favourite/{id}', 'FavouriteController@create')->name('addToFavourite')->middleware('user');
    Route::get('/favourite/list', 'FavouriteController@index')->name('favourite.list')->middleware('user');
    Route::get('/delete/favourite/{id}', 'FavouriteController@destroy')->name('delete.favourite')->middleware('user');
});