<?php

return [
    'text_heading' => 'আপনার কয়েকটি পছন্দকে হ্যালো বলে শুরু করুন',
    'update_porfile' => 'আপনার প্রোফাইল আপডেট করুন',
    'update_profile_sub' => 'এখনই এটি আপডেট করুন',
    'set_preference' => 'আপনার পছন্দগুলি সেট করুন',
    'set_preference_sub' => 'এটি এখন সেট করুন',
    'slider_text' => 'ড্যাশবোর্ড',
    'chat' => 'চ্যাঁট',
];