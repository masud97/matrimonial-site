<?php

return [
    'home' => 'হোম',
    'dashboard' => 'ড্যাশবোর্ড',
    'chat' => 'চ্যাঁট',
    'favourite' => 'পছন্দের লিস্ট',
    'logout' => 'লগ আউট',
];