<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    */

    'form_title' => 'To have & to hold...',
    'form_placeholder' => 'Meet a Bride / Groom',
    'form_btn_text' => "Let's Begin",
    'feature_1' => 'Registration',
    'feature_2' => 'Searching',
    'feature_3' => 'Connect',
    'feature_4' => 'Interact',
    'slider_title' => 'Happy Stories',
    'why_us' => 'Why Us',
    'need_help' => 'Need Help?',
    'login' => 'Please Login',
    'login_username' => 'Mobile no. / Email',
    'login_password' => 'Password',
    'stay_signedin' => 'Stay Signed in',
    'sign_in' => 'SIGN IN',
    'forgot_password' => 'Forgot Password?',
    'sign_up_free' => 'Sign Up Free',
    'mobile' => 'Mobile No.',
    'email' => 'Email',
    'welcome' => "Welcome! Let's start",
    're_password' => 'Confirm Password',
    'create_profile_for' => 'Create profile for',
    'sign_up' => 'Sign Up',
    'by_signing_up' => 'By signing up, you agree to our Terms',
    'already_member' => 'Already Member! Login',
    'forgot_password_email' => 'Please enter email address',
    'submit' => 'Submit',
];