<?php

return [
    'text_heading' => 'Get started by saying hello to some of your Matches',
    'update_porfile' => 'Update Your Profile',
    'update_profile_sub' => 'Update it now',
    'set_preference' => 'Set Your Preferences',
    'set_preference_sub' => 'Set it now',
    'slider_text' => 'Dashboard',
    'chat' => 'Chat',
];