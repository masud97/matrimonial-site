<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    */

    'menu_item_1' => 'Dashboard',
    'menu_item_2' => 'Chat',
    'menu_item_3' => 'Login',
    'menu_item_4' => 'Logout',
    'menu_item_5' => 'Register Free',
    'home' => 'Home',
];