<?php

return [
    'home' => 'Home',
    'dashboard' => 'Dashboard',
    'chat' => 'Chat',
    'favourite' => 'Favourite',
    'logout' => 'Logout',
];