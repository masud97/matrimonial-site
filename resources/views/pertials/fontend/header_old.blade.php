<div class="container">
    <div class="row align-items-center justify-content-between d-flex">
        <div id="logo">
            <a href="index.html"><img src="{{url('assets/img/logo.png')}}" alt="" title="" /></a>
        </div>
        <ul class="nav-menu">
            <li><a class="log-menu" href="#" data-toggle="modal" data-target="#login_modal">Login <i class="fa fa-user-circle-o" aria-hidden="true"></i></a></li>
            <li><a class="ticker-btn" href="#" data-toggle="modal" data-target="#register_modal">Register Free</a></li>
        </ul>
    </div>
</div>