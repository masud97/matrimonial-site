<script src="{{url('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/vendor/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{url('assets/js/easing.min.js')}}"></script>
<script src="{{url('assets/js/hoverIntent.js')}}"></script>
<script src="{{url('assets/js/superfish.min.js')}}"></script>
<script src="{{url('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.sticky.js')}}"></script>
<script src="{{url('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('assets/js/parallax.min.js')}}"></script>
<script src="{{url('assets/js/mail-script.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="38d45c5b43721184da46bd14-text/javascript"></script>
<script type="38d45c5b43721184da46bd14-text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
<script src="{{url('assets/js/rocket-loader.min.js')}}" data-cf-settings="38d45c5b43721184da46bd14-|49" defer=""></script>
<script type="text/javascript">
    
    //on select hide and show
    $(function () {
        
        //Search Hide and show
        $(".search-btn").click(function() {
            $(".search-box").hide();
            $(".target").slideToggle("slow");
        });
        $(".search-input-box").click(function() {
            $(".search-box").hide();
            $(".target").slideToggle("slow");
        });
        //Search Hide and show
        
        
        
    });
    //on select hide and show
            
</script>

<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}

<script>
    @if($errors->any())
        @foreach($errors->all() as $error)
        toastr.error('{{ $error }}', 'Error', {
                "closeButton": true,
                "progressBar": true
        });
        
        @endforeach
        $(window).on('load',function(){
        var form1Id = sessionStorage.getItem('form1');
        var form2Id = sessionStorage.getItem('form2');
        console.log(form1Id);
        console.log(form2Id);

        if(form1Id != null && form1Id == 'form1'){
            $('#login_modal').modal('show');
        }

        if(form2Id != null && form2Id == 'form2'){
            $('#register_modal').modal('show');
        }
          
    });
@endif
</script>
<script>
    $('#for-whome').change(function(){      
        $('.select-gender').show("slow");
        var whome = $(this).val(); 
        var $setgender = $('#set-gender');
        $setgender.empty();
        if(whome == 1){                      
            $setgender.html('<option value="male">Male</option><option value="female">Female</option>');        
            $setgender.niceSelect('update');
        }else if(whome == 2 || whome == 4){            
            $setgender.html('<option value="male" selected >Male</option>');
            $setgender.niceSelect('update');
        }else if(whome == 3 || whome == 5){            
            $setgender.html('<option value="female" selected >Female</option>');
            $setgender.niceSelect('update');
        }else{            
            $setgender.html('<option value="male">Male</option><option value="female">Female</option>');
            $setgender.niceSelect('update');        
        }
    });
</script>
<script>
    function hidelogin(){
        $('#login_modal').modal('toggle');
    }

    function hideregistration(){
        $('#register_modal').modal('toggle');
    }

    function signin_status(){
        // Check browser support
        if (typeof(Storage) !== "undefined") {
        sessionStorage.clear();
        // Store
        sessionStorage.setItem("form1", "form1");
        }
    }

    function signup_status(){
        // Check browser support
        if (typeof(Storage) !== "undefined") {
        sessionStorage.clear();
        // Store
        sessionStorage.setItem("form2", "form2");
        }
    }
</script>

<script>
    $('#select-locale').change(function(){
        var selectLocale = $('#select-locale').val();
        var url = "{{url('/locale')}}/" + selectLocale;
        console.log(url);

        $.ajax({
            url:url,
            method:'GET',
        }).done(function(data){
            console.log(data.msg);
            if(data.msg == 'bn'){
                $("#bangla").attr("selected",true);
                $("#english").attr("selected",false);
            }else{
                $("#bangla").attr("selected",false);
                $("#english").attr("selected",true);
            }
            location.reload(true);
        });
    });
</script>