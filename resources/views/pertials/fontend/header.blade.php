<header id="header" id="home">
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="index.html"><img src="{{url('assets/img/logo.png')}}" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">											
				<ul class="nav-menu">
					@if (\Request::is('visitor/search'))  
					<li><a class="log-menu" href="{{url('/')}}">{{trans('index.home')}}</a></li>
					@endif
					@if(Auth::user())<li><a class="log-menu" href="{{route('user.dashboard')}}">{{trans('index.menu_item_1')}}</a></li>@endif
					@if(Auth::user())<li><a class="log-menu" href="{{route('user.chat')}}">{{trans('index.menu_item_2')}}</a></li>@endif
					@if(Auth::guest())<li><a class="log-menu" href="#" data-toggle="modal" data-target="#login_modal">{{trans('index.menu_item_3')}} <i class="fa fa-user-circle-o" aria-hidden="true"></i></a></li>@endif
					@if(Auth::user())
						<li>
						<a class="log-menu" 
							href="{{ route('logout') }}" 
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							{{trans('index.menu_item_4')}}</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</li>
					@endif
					@if(Auth::guest())<li><a class="log-menu" href="#" data-toggle="modal" data-target="#register_modal">{{trans('index.menu_item_5')}}</a></li>@endif					
				</ul>
				<select name="locale" id="select-locale" class="language-select-btn">
					<option>Language</option>
					<option id="english" value="en">English</option>
					<option id="bangla" value="bn">বাংলা</option>
				</select>
			</nav>
		</div>
	</div>
</header>
