<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="{{url('assets/img/favicon-16.png')}}">
<meta name="author" content="Mortuja Jahan, S. M. Masud Rana">
<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="UTF-8">
<title>Marriage Matrimony</title>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="https://fonts.googleapis.com/css?family=Roboto:100,200,400,300,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Petit+Formal+Script&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{url('assets/css/linearicons.css')}}">
<link rel="stylesheet" href="{{url('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{url('assets/css/nice-select.css')}}">
<link rel="stylesheet" href="{{url('assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/datepicker.css')}}">
<link rel="stylesheet" href="{{url('assets/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{url('assets/css/main.css')}}">
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">