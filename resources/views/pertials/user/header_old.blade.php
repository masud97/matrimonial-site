<div class="container">
    <div class="row align-items-center justify-content-between d-flex">
        <div id="logo">
        <a href="{{url('/dashboard')}}"><img src="{{url('assets/img/logo.png')}}" alt="Logo" /></a>
        </div>
        <!--logout menu-->
        <span class="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
        <button id="hamburger-menu" data-toggle="ham-navigation" class="hamburger-menu-button">
        </button>
        <nav id="ham-navigation" class="ham-menu">
          <ul class="menu">
            <li>
            <a class="ham" 
                href="{{ route('logout') }}" 
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
          </ul>              
        </nav>
        <!--logout menu-->
    </div>
</div>