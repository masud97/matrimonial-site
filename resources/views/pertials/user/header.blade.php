<header id="header" id="home">
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="index.html"><img src="{{url('assets/img/logo.png')}}" alt="" title="" /></a>
                </div>
                <!--logout menu-->
                <span class="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                <button id="hamburger-menu" data-toggle="ham-navigation" class="hamburger-menu-button">
                </button>
                <nav id="ham-navigation" class="ham-menu">
                  <ul class="menu">
                  <li><a class="ham" href="{{url('/')}}">{{trans('header.home')}}</a></li>
                    <li><a class="ham" href="{{route('user.dashboard')}}">{{trans('header.dashboard')}}</a></li>
                    <li><a class="ham" href="{{route('user.chat')}}">{{trans('header.chat')}}</a></li>
                    <li><a class="ham" href="{{route('user.notifications.view')}}"><div id="viewChatRequestCount">0</div></a></li>
                    <li><a class="ham" href="{{route('user.favourite.list')}}">{{trans('header.favourite')}}</a></li>
                    <li>
						<a class="ham" 
							href="{{ route('logout') }}" 
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							{{trans('header.logout')}}</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
					
					</li>
                  </ul>
                </nav>
                <!--logout menu-->
            </div>
        </div>
    </header>





