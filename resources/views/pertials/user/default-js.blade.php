<!--

<script src="{{url('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/vendor/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{url('assets/js/easing.min.js')}}"></script>
<script src="{{url('assets/js/hoverIntent.js')}}"></script>
<script src="{{url('assets/js/superfish.min.js')}}"></script>
<script src="{{url('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.sticky.js')}}"></script>
<script src="{{url('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('assets/js/parallax.min.js')}}"></script>
<script src="{{url('assets/js/mail-script.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="38d45c5b43721184da46bd14-text/javascript"></script>
<script src="{{url('assets/js/toastr.min.js')}}"></script>
{!! Toastr::message() !!}

<script>
    @if($errors->any())
            @foreach($errors->all() as $error)
            toastr.error('{{ $error }}', 'Error', {
                    "closeButton": true,
                    "progressBar": true
            });
            @endforeach
    @endif
</script>
<script type="38d45c5b43721184da46bd14-text/javascript">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23581568-13');
    </script>
    <script src="{{url('assets/js/rocket-loader.min.js')}}" data-cf-settings="38d45c5b43721184da46bd14-|49" defer=""></script>  
      
    <script>
        var button = document.getElementById('hamburger-menu'),
        span = button.getElementsByTagName('span')[0];
        button.onclick =  function() {
          span.classList.toggle('hamburger-menu-button-close');
        };
        $('#hamburger-menu').on('click', toggleOnClass);
        function toggleOnClass(event) {
          var toggleElementId = '#' + $(this).data('toggle'),
          element = $(toggleElementId);
          element.toggleClass('on');
        }
        // close hamburger menu after click a
        $( '.menu li a' ).on("click", function(){
          $('#hamburger-menu').click();
        });
    </script>

-->

<script src="{{url('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/vendor/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{url('assets/js/easing.min.js')}}"></script>
<script src="{{url('assets/js/hoverIntent.js')}}"></script>
<script src="{{url('assets/js/superfish.min.js')}}"></script>
<script src="{{url('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.sticky.js')}}"></script>
<script src="{{url('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('assets/js/parallax.min.js')}}"></script>
<script src="{{url('assets/js/mail-script.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="38d45c5b43721184da46bd14-text/javascript"></script>
    <script type="38d45c5b43721184da46bd14-text/javascript">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23581568-13');
    </script>
    <script src="js/rocket-loader.min.js" data-cf-settings="38d45c5b43721184da46bd14-|49" defer=""></script>  
      
    <script>
    $(function () {
        $('.input-group.date').datepicker({format: "dd-mm-yyyy"});
    });   
        var button = document.getElementById('hamburger-menu'),
        span = button.getElementsByTagName('span')[0];
        button.onclick =  function() {
          span.classList.toggle('hamburger-menu-button-close');
        };
        $('#hamburger-menu').on('click', toggleOnClass);
        function toggleOnClass(event) {
          var toggleElementId = '#' + $(this).data('toggle'),
          element = $(toggleElementId);
          element.toggleClass('on');
        }
        // close hamburger menu after click a
        $( '.menu li a' ).on("click", function(){
          $('#hamburger-menu').click();
        });
    </script>
    <script type="text/javascript">
        //Spouse button-modal
        $('.button-open-modal').click(function() {
              $(this).hide();
                $('.front').addClass('front-open');
                $('.back').addClass('back-open');
                $('.opened').addClass('opened-open');
                $('.client_modal').show();
                setTimeout(function() {
                    $('.modal').addClass('shadow');
                }, 1000);
                setTimeout(function() {
                    $('.front').removeClass('front-open');
                    $('.back').removeClass('back-open');
                    $('.opened').removeClass('opened-open');
                }, 1200);
              $('.wrapper').delay(500).fadeIn();
            });
            $('.close').click(function() {
                $('.wrapper').fadeOut(300);
                $('.client_modal').removeClass('shadow');
                $('.front').addClass('front-close');
                $('.back').addClass('back-close');
                $('.opened').addClass('opened-close');
                setTimeout(function() {
                    $('.client_modal').hide();
                    $('.button-open-modal').show();
                    $('.front').removeClass('front-close');
                    $('.back').removeClass('back-close');
                    $('.opened').removeClass('opened-close');
                }, 1100)
            });
            $('.wrapper').click(function() {
                $('.wrapper').fadeOut(300);
                $('.client_modal').removeClass('shadow');
                $('.front').addClass('front-close');
                $('.back').addClass('back-close');
                $('.opened').addClass('opened-close');
                setTimeout(function() {
                    $('.client_modal').hide();
                    $('.button-open-modal').show();
                    $('.front').removeClass('front-close');
                    $('.back').removeClass('back-close');
                    $('.opened').removeClass('opened-close');
                }, 1100)
            });
        //Spouse button-modal
        
        
        //Start Profile Image Slider
        jQuery(document).ready(function ($) {
            var slideCount = $('.profile_slider ul li').length;
            var slideWidth = $('.profile_slider ul li').width();
            var slideHeight = $('.profile_slider ul li').height();
            var sliderUlWidth = slideCount * slideWidth;

            $('.profile_slider').css({ width: slideWidth, height: slideHeight });
            $('.profile_slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
            $('.profile_slider ul li:last-child').prependTo('.profile_slider ul');

            function moveLeft() {
                $('.profile_slider ul').animate({
                    left: + slideWidth
                }, 200, function () {
                    $('.profile_slider ul li:last-child').prependTo('.profile_slider ul');
                    $('.profile_slider ul').css('left', '');
                });
            };

            function moveRight() {
                $('.profile_slider ul').animate({
                    left: - slideWidth
                }, 200, function () {
                    $('.profile_slider ul li:first-child').appendTo('.profile_slider ul');
                    $('.profile_slider ul').css('left', '');
                });
            };

            $('a.control_prev').click(function () {
                moveLeft();
            });

            $('a.control_next').click(function () {
                moveRight();
            });

        }); 
        //End Profile Image Slider
        
    </script>
@stack('js')