<!--
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="{{url('assets/img/favicon-16.png')}}">
<meta name="author" content="Mortuja Jahan & S. M. Masud Rana">
<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="UTF-8">
<title>
    @yield('title')
</title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="{{url('assets/css/linearicons.css')}}">
<link rel="stylesheet" href="{{url('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{url('assets/css/nice-select.css')}}">
<link rel="stylesheet" href="{{url('assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/owl.carousel.css')}}">
<link href="{{url('assets/css/datepicker.css')}}" rel="stylesheet"/>
<link href="{{url('assets/css/foundation.min.css')}}" rel="stylesheet"/>
<link href="{{url('assets/css/materialdesignicons.min.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{url('assets/css/main.css')}}">
<link rel="stylesheet" href="{{ url('assets/css/toastr.min.css')}}">
-->

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="{{url('assets/img/favicon-16.png')}}">
<meta name="author" content="Mortuja Jahan & S. M. Masud Rana">
<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="UTF-8">
<title>@yield('title')</title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="{{url('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{url('assets/css/nice-select.css')}}">
<link rel="stylesheet" href="{{url('assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/owl.carousel.css')}}">
<link href="{{url('assets/css/datepicker.css')}}" rel="stylesheet"/>
<link href="{{url('assets/css/foundation.min.css')}}" rel="stylesheet"/>
<link href="{{url('assets/css/materialdesignicons.min.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{url('assets/css/main.css')}}">
@stack('css')