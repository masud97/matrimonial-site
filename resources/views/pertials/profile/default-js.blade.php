<script src="{{url('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/vendor/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{url('assets/js/easing.min.js')}}"></script>
<script src="{{url('assets/js/hoverIntent.js')}}"></script>
<script src="{{url('assets/js/superfish.min.js')}}"></script>
<script src="{{url('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.sticky.js')}}"></script>
<script src="{{url('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('assets/js/parallax.min.js')}}"></script>
<script src="{{url('assets/js/mail-script.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="38d45c5b43721184da46bd14-text/javascript"></script>
<script src="{{url('assets/js/toastr.min.js')}}"></script>
{!! Toastr::message() !!}

<script>
    @if($errors->any())
            @foreach($errors->all() as $error)
            toastr.error('{{ $error }}', 'Error', {
                    "closeButton": true,
                    "progressBar": true
            });
            @endforeach
    @endif
</script>

<script type="38d45c5b43721184da46bd14-text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>

<script type="text/javascript">
    $(function () {
        $('.input-group.date').datepicker({format: "dd-mm-yyyy"});
    });        
    
    //hide show
    $(function () {
        //Religion select                 
            $(".religion-div").hide();
        //Religion select

        //Marital status
        $('#marital_status').change(function () {
            var select=$(this).find(':selected').val();        
            $(".has-child").hide("slow");
            $('#' + select).show("slow");
        }).change();
        //Marital status       
        
        
        //country select
        $('.country-bd-select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".bd-district").hide("slow");
            $(".bd-adress-area").hide("slow");
            $('#' + select).show("slow");
        }).change();
        $('.dis_select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".bd-adress-area").hide("slow");
            $('#' + select).show("slow");
        }).change(); 
        
        $('.country-ame-select').on('change', function() {
            $('.ame-state').css('display', 'none');
            if ( $(this).val() != 18 ) {
            $('.ame-state').slideToggle().css('display', 'block');
            }else{
                $('.bd-district').slideToggle().css('display', 'block'); 
            }
        });
    
        $('.origin-select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".home-origin").hide("slow");
            $('#' + select).show("slow");
        }).change(); 
    
        $('.education-level').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".edu-detail").hide("slow");
            $('#' + select).show("slow");
        }).change();  
    
        $('.emp-in').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".other-emp-area").hide("slow");
            $('#' + select).show("slow");
        }).change();  
    
        $('.occ').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".other-occ-area").hide("slow");
            $('#' + select).show("slow");
        }).change();         
        
        //country select 
        
    });
    //hide show
    
    
    //Image Upload Area        
    function readURL(input, imgControlName) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(imgControlName).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imag").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview";
        readURL(this, imgControlName);
        $('.preview1').addClass('it');
        $('.btn-rmv1').addClass('rmv');
    });
    $("#imag2").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview2";
        readURL(this, imgControlName);
        $('.preview2').addClass('it');
        $('.btn-rmv2').addClass('rmv');
    });
    $("#removeImage1").click(function(e) {
        e.preventDefault();
        $("#imag").val("");
        $("#ImgPreview").attr("src", "");
        $('.preview1').removeClass('it');
        $('.btn-rmv1').removeClass('rmv');
    });
    $("#removeImage2").click(function(e) {
        e.preventDefault();
        $("#imag2").val("");
        $("#ImgPreview2").attr("src", "");
        $('.preview2').removeClass('it');
        $('.btn-rmv2').removeClass('rmv');
    });
    //Image Upload Area
    
    
    //Wizard area Js
    $(document).ready(function () {
    // Checking button status ( wether or not next/previous and
    // submit should be displayed )
    const checkButtons = (activeStep, stepsCount) => {
        const prevBtn = $("#wizard-prev");
        const nextBtn = $("#wizard-next");
        const submBtn = $("#wizard-subm");

        switch (activeStep / stepsCount) {
        case 0: // First Step
            prevBtn.hide();
            submBtn.hide();
            nextBtn.show();
            break;
        case 1: // Last Step
            nextBtn.hide();
            prevBtn.show();
            submBtn.show();
            break;
        default:
            submBtn.hide();
            prevBtn.show();
            nextBtn.show();
        }
    };
    // Scrolling the form to the middle of the screen if the form
    // is taller than the viewHeight
    const scrollWindow = (activeStepHeight, viewHeight) => {
        if (viewHeight < activeStepHeight) {
        $(window).scrollTop($(steps[activeStep]).offset().top - viewHeight / 2);
        }
    };
    // Setting the wizard body height, this is needed because
    // the steps inside of the body have position: absolute
    const setWizardHeight = activeStepHeight => {
        $(".wizard-body").height(activeStepHeight);
    };
    $(function() {
        // Form step counter (little cirecles at the top of the form)
        const wizardSteps = $(".wizard-header .wizard-step");
        // Form steps (actual steps)
        const steps = $(".wizard-body .step");
        // Number of steps (counting from 0)
        const stepsCount = steps.length - 1;
        // Screen Height
        const viewHeight = $(window).height();
        // Current step being shown (counting from 0)
        let activeStep = 0;
        // Height of the current step
        let activeStepHeight = $(steps[activeStep]).height();
        checkButtons(activeStep, stepsCount);
        setWizardHeight(activeStepHeight);
        // Resizing wizard body when the viewport changes
        $(window).resize(function() {
        setWizardHeight($(steps[activeStep]).height());
        });
        // Previous button handler
        $("#wizard-prev").click(() => {
        // Sliding out current step
        $(steps[activeStep]).removeClass("active");
        $(wizardSteps[activeStep]).removeClass("active");
        activeStep--;
        // Sliding in previous Step
        $(steps[activeStep]).removeClass("off").addClass("active");
        $(wizardSteps[activeStep]).addClass("active");

        activeStepHeight = $(steps[activeStep]).height();
        setWizardHeight(activeStepHeight);
        checkButtons(activeStep, stepsCount);
        });
        // Next button handler
        $("#wizard-next").click(() => {
        // Sliding out current step
        $(steps[activeStep]).removeClass("inital").addClass("off").removeClass("active");
        $(wizardSteps[activeStep]).removeClass("active");
        // Next step
        activeStep++;
        // Sliding in next step
        $(steps[activeStep]).addClass("active");
        $(wizardSteps[activeStep]).addClass("active");

        activeStepHeight = $(steps[activeStep]).height();
        setWizardHeight(activeStepHeight);
        checkButtons(activeStep, stepsCount);
            
        
        });
    });    
    });

    //Wizard area Js

    
</script>
<script>
    var button = document.getElementById('hamburger-menu'),
    span = button.getElementsByTagName('span')[0];
    button.onclick =  function() {
        span.classList.toggle('hamburger-menu-button-close');
    };
    $('#hamburger-menu').on('click', toggleOnClass);
    function toggleOnClass(event) {
        var toggleElementId = '#' + $(this).data('toggle'),
        element = $(toggleElementId);
        element.toggleClass('on');
    }
    // close hamburger menu after click a
    $( '.menu li a' ).on("click", function(){
        $('#hamburger-menu').click();
    });
</script>

<Script>
//Religion select
$('#religion').change(function () {
    var religion=$('#religion').val();    
    var url = "{{url('religion')}}/" + religion;
    console.log(url);
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            $('.religion-div').show("slow");
            var $caste = $('#caste');
            $caste.empty();
            $caste.append('<option value="#" selected disable>Select Caste</option>');
            for (var i = 0; i < data.castes.length; i++) {
                $caste.append('<option value=' + data.castes[i]['id'] + '>' + data.castes[i]['english_name'] + '</option>');
                $caste.niceSelect('update');
            }
        });        
})
//Religion select
</Script>

<script>
    function resendVerificationCode(){
        var url = "{{url('resend/code')}}";      
        console.log(url);
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {    
            toastr.success("New verification code has sent to your email.");
        });
    }

</script>