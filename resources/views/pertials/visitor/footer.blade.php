<div class="container">
        <div class="row">
            <div class="col-lg-6  col-md-12">
                <div class="single-footer-widget newsletter">
                    <h6>Why Us</h6>
                    <p>“Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government.</p>
                </div>
            </div>
            <div class="col-lg-3  col-md-12">
                <div class="single-footer-widget">
                    <h6>Need Help?</h6>
                    <ul class="footer-nav">
                        <li><a href="#">Member LogIn</a></li>
                        <li><a href="#">Sign Up</a></li>
                        <li><a href="#">Advanched Search</a></li>
                        <li><a href="#">Success Stories</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3  col-md-12">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">New Member ...</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><a href="#"><img src="{{url('assets/img/i1.jpg')}}" alt="img"></a></li>
                        <li><a href="#"><img src="{{url('assets/img/i2.jpg')}}" alt="img"></a></li>
                        <li><a href="#"><img src="{{url('assets/img/i3.jpg')}}" alt="img"></a></li>
                        <li><a href="#"><img src="{{url('assets/img/i4.jpg')}}" alt="img"></a></li>
                        <li><a href="#"><img src="{{url('assets/img/i5.jpg')}}" alt="img"></a></li>
                        <li><a href="#"><img src="{{url('assets/img/i6.jpg')}}" alt="img"></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row footer-bottom d-flex justify-content-between">
            <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">
            Copyright &copy;<script type="38d45c5b43721184da46bd14-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved by <a href="http://www.bigweb.com.bd/" target="_blank">Big Web Technologies Ltd.</a>
            </p>
        </div>
    </div>