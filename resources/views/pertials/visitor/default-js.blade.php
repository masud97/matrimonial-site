<script src="{{url('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/vendor/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{url('assets/js/easing.min.js')}}"></script>
<script src="{{url('assets/js/hoverIntent.js')}}"></script>
<script src="{{url('assets/js/superfish.min.js')}}"></script>
<script src="{{url('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.sticky.js')}}"></script>
<script src="{{url('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('assets/js/parallax.min.js')}}"></script>
<script src="{{url('assets/js/mail-script.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="38d45c5b43721184da46bd14-text/javascript"></script>
<script type="38d45c5b43721184da46bd14-text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>

<script>
    var button = document.getElementById('hamburger-menu'),
    span = button.getElementsByTagName('span')[0];
    button.onclick =  function() {
      span.classList.toggle('hamburger-menu-button-close');
    };
    $('#hamburger-menu').on('click', toggleOnClass);
    function toggleOnClass(event) {
      var toggleElementId = '#' + $(this).data('toggle'),
      element = $(toggleElementId);
      element.toggleClass('on');
    }
    // close hamburger menu after click a
    $( '.menu li a' ).on("click", function(){
      $('#hamburger-menu').click();
    });
</script>
<script type="text/javascript">
    //Spouse button-modal
    $('.button-open-modal').click(function() {
          $(this).hide();
            $('.front').addClass('front-open');
            $('.back').addClass('back-open');
            $('.opened').addClass('opened-open');
            $('.client_modal').show();
            setTimeout(function() {
                $('.modal').addClass('shadow');
            }, 1000);
            setTimeout(function() {
                $('.front').removeClass('front-open');
                $('.back').removeClass('back-open');
                $('.opened').removeClass('opened-open');
            }, 1200);
          $('.wrapper').delay(500).fadeIn();
        });
        $('.close').click(function() {
            $('.wrapper').fadeOut(300);
            $('.client_modal').removeClass('shadow');
            $('.front').addClass('front-close');
            $('.back').addClass('back-close');
            $('.opened').addClass('opened-close');
            setTimeout(function() {
                $('.client_modal').hide();
                $('.button-open-modal').show();
                $('.front').removeClass('front-close');
                $('.back').removeClass('back-close');
                $('.opened').removeClass('opened-close');
            }, 1100)
        });
        $('.wrapper').click(function() {
            $('.wrapper').fadeOut(300);
            $('.client_modal').removeClass('shadow');
            $('.front').addClass('front-close');
            $('.back').addClass('back-close');
            $('.opened').addClass('opened-close');
            setTimeout(function() {
                $('.client_modal').hide();
                $('.button-open-modal').show();
                $('.front').removeClass('front-close');
                $('.back').removeClass('back-close');
                $('.opened').removeClass('opened-close');
            }, 1100)
        });
    //Spouse button-modal
    
    
    //Start Profile Image Slider
    jQuery(document).ready(function ($) {
        var slideCount = $('.profile_slider ul li').length;
        var slideWidth = $('.profile_slider ul li').width();
        var slideHeight = $('.profile_slider ul li').height();
        var sliderUlWidth = slideCount * slideWidth;

        $('.profile_slider').css({ width: slideWidth, height: slideHeight });
        $('.profile_slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
        $('.profile_slider ul li:last-child').prependTo('.profile_slider ul');

        function moveLeft() {
            $('.profile_slider ul').animate({
                left: + slideWidth
            }, 200, function () {
                $('.profile_slider ul li:last-child').prependTo('.profile_slider ul');
                $('.profile_slider ul').css('left', '');
            });
        };

        function moveRight() {
            $('.profile_slider ul').animate({
                left: - slideWidth
            }, 200, function () {
                $('.profile_slider ul li:first-child').appendTo('.profile_slider ul');
                $('.profile_slider ul').css('left', '');
            });
        };

        $('a.control_prev').click(function () {
            moveLeft();
        });

        $('a.control_next').click(function () {
            moveRight();
        });

    }); 
    //End Profile Image Slider
    
</script>

<script>
    function show_details(id){
        var url = "{{url('/visitor/showdetails')}}/" + id;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {            
            $('#popup1').modal('show');  

            console.log("Image: "+data.details.image);          

            var today = new Date();
            var dob = new Date(data.details.dob);
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));

            

            var $nameField = $('.candidate-name');
            $nameField.empty();
            $nameField.append( data.details.full_name );           

            var $personalDetails = $('.personal-details');
            $personalDetails.empty();
            $personalDetails.append( data.details.personal_details );

            var $age = $('.age');
            $age.empty();
            $age.append( age + ' Years' );

            var $height = $('.height');
            $height.empty();
            $height.append( toFeet(data.details.height) );

            var $religion = $('.religion');
            $religion.empty();
            $religion.append( data.details.religion.english_name );

            var $caste = $('.caste');
            $caste.empty();
            $caste.append( data.details.caste.english_name );

            var $address = $('.address');
            $address.empty();
            $address.append( data.details.present_district.english_name );

            var $education = $('.education');
            $education.empty();
            $education.append( data.details.education.english_name );

            var $profession = $('.profession');
            $profession.empty();
            $profession.append( data.details.employe_type.english_name );

            var $profileimg = $('.profileimg');
            $profileimg.empty();
            $profileimg.append("<img src='{{url('assets/img/profile')}}/"+ data.details.image +"' />");

            var $album1 = $('.album-1');
            $album1.empty();
            $album1.append("<img src='{{url('assets/img/album')}}/"+ data.details.album_1 +"' />");

            var $album2 = $('.album-2');
            $album2.empty();
            $album2.append("<img src='{{url('assets/img/album')}}/"+ data.details.album_2 +"' />");
            

        });
    }

    function toFeet(n) {
      var realFeet = ((n*0.393700) / 12);
      var feet = Math.floor(realFeet);
      var inches = Math.round((realFeet - feet) * 12);
      return feet + " feet " + inches + ' inches';
    }

</script>