@extends('layouts.fontend')

@section('content')

<!--Start Modal Area-->
<div class="container text-center">
  <div class="modal fade" id="login_modal" tabindex="-1">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
            <div class="p-3">
                <a href="#" class="modal-close-btn" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
                <div class="modal_logo">                    
                    <img src="{{url('assets/img/modal-logo.png')}}" alt="" title="" />
                </div>                
                <h5>{{trans('welcome.login')}}</h5> 
                <form method="POST" action="{{ route('login') }}" id="from_1">
                    @csrf
                                   
                <div class="text-left">
                    <div class="mt-10">
                        <input type="text" id="email" name="email" placeholder="{{trans('welcome.login_username')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Mobile No. / Email'" required class="single-input @error('email') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="mt-10">
                        <input type="password" id="password" name="password" placeholder="{{trans('welcome.login_password')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Password'" required class="single-input @error('password') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <br>
                    <div class="switch-wrap d-flex justify-content-between">                        
                        <p>{{trans('welcome.stay_signedin')}}</p>
                        <div class="primary-checkbox">
                            <input type="checkbox" id="default-checkbox" name="remember">
                            <label for="default-checkbox"></label>
                        </div>                        
                    </div>                   
                </div>                
                <div class="close_menu">
                    <button type="submit" name="submit" class="modal-ticker-btn" onclick="signin_status()">{{trans('welcome.sign_in')}}</button>                    
                </div>
                </form>
                <small><a href="" data-toggle="modal" data-target="#forget_password_modal" onclick="hidelogin()">{{trans('welcome.forgot_password')}}</a></small> 
                <a href="#" data-toggle="modal" data-target="#register_modal" class="small sign_up_free" onclick="hidelogin()">{{trans('welcome.sign_up_free')}}</a> 
            </div>
      </div>
    </div>
  </div>
</div>




<!--Start Forget modal-->
<div class="container text-center">
  <div class="modal fade" id="forget_password_modal" tabindex="-1">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
            <div class="p-3">
                <a href="#" class="modal-close-btn" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
                <div class="modal_logo">                    
                    <img src="{{url('assets/img/modal-logo.png')}}" alt="" title="" />
                </div>                
            <h5>{{trans('welcome.forgot_password_email')}}</h5> 
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf               
                <div class="text-left">
                    <div class="mt-10">
                        <input type="text" id="email" name="email" placeholder="{{trans('welcome.email')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Email'" required class="single-input @error('email') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>                            
                </div>                
                <div class="close_menu">
                    <button type="submit" name="submit" class="modal-ticker-btn">{{trans('welcome.submit')}}</button>                    
                </div>
                </form>               
            </div>
      </div>
    </div>
  </div>
</div>





<div class="container text-center">
  <div class="modal fade" id="register_modal" tabindex="-1">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">            
            <div class="p-3">
                <a href="#" class="modal-close-btn" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
                <br>
                <h5>{{trans('welcome.welcome')}}</h5>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                <div class="text-left"> 
                    <div class="mt-10">
                        <input type="text" name="email" placeholder="{{trans('welcome.email')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Email'" required class="single-input @error('email') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>                  
                    <div class="mt-10">
                        <input type="text" id="mobile" name="mobile" placeholder="{{trans('welcome.mobile')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Mobile Number'" required class="single-input @error('mobile') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="mt-10">
                        <input type="password" name="password" placeholder="{{trans('welcome.login_password')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Password'" required class="single-input @error('password') is-invalid @enderror" data-cf-modified-c10bb454cc45607171f3df27-="">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div> 
                    <div class="mt-10">
                        <input type="password" name="password_confirmation" placeholder="{{trans('welcome.re_password')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Re-Password'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                    </div> 
                    <div class="mt-10">
                       <div class="form-select">
                            <select id="for-whome" class="option-select g1_select @error('for_whome') is-invalid @enderror" name="for_whome">
                                <option>{{trans('welcome.create_profile_for')}}</option>
                                @foreach($forWhomeList as $list)
                                <option value="{{ $list->id }}">{{ $list->en_for_whome }}</option>
                                @endforeach
                            </select>
                            @error('for_whome')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                       </div>
                    </div>

                    
                    <div class="mt-10 select-gender" style="display: none;">
                       <div class="form-select">
                           <select name="gender" class="g2_select @error('gender') is-invalid @enderror" id="set-gender">
                               
                           </select>
                           @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                       </div>
                    </div>                   

                </div>
                <div class="close_menu">
                    <button type="submit" name="submit" class="modal-ticker-btn" onclick="signup_status()">{{trans('welcome.sign_up')}}</button>
                </div>
                </form>

                <small>By signing up, you agree to our</small> 
                <a href="#" class="small">Terms</a><br>
                <small>{{trans('welcome.already_member')}}</small> 
                <a href="#" data-toggle="modal" data-target="#login_modal" onclick="hideregistration()" class="small sign_up_free">{{trans('welcome.login')}}</a> 
            </div>
      </div>
    </div>
  </div>
</div>
<!--End Modal Area-->

<!--Home Page Banner Area-->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row fullscreen d-flex align-items-center justify-content-center">
            <div class="banner-content col-lg-12">
            <h1 class="text-white">{{trans('welcome.form_title')}} </h1>

            <form method="POST" action="{{route('visitor.search')}}" class="serach-form-area">                   
                @csrf
                <div class="row justify-content-center form-wrap search-box">
                    <div class="col-md-10 form-cols home-search-input">
                        <div class="search-input-box">
                        <input type="text" name="first_name" placeholder="{{trans('welcome.form_placeholder')}}" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Meet a bride / groom'" disabled class="single-input-primary">
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-search-btn">
                    <button type="button" class="btn btn-info search-btn">{{trans('welcome.form_btn_text')}}</button>
                    </div>
                </div>
                <div class="row justify-content-center form-wrap target"  style="display: none;">
                    <div class="col-md-2 form-cols home-select-search looking-select">
                        <div class="default-select">
                            <select name="gender" class="gender">
                                <option value="1">Looking for a</option>
                                <option value="male">Bride</option>
                                <option value="female">Groom</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-select-search">
                        <div class="default-select aged-select">
                            <span class="aged-first">Aged</span> 
                            <select name="age-from">
                                <option value="18">18</option>
                                <option value="20">20</option>
                                <option value="22">22</option>
                                <option value="24">24</option>
                                <option value="26">26</option>
                                <option value="28">28</option>
                                <option value="30">30</option>
                                <option value="32">32</option>
                                <option value="34">34</option>
                                <option value="36">36</option>
                                <option value="38">38</option>
                                <option value="40">40</option>                                
                            </select>                            
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-select-search">
                        <div class="default-select aged2-select">
                            <span class="aged-second">to</span>
                            <select name="age-to">
                                <option value="20">20</option>
                                <option value="22">22</option>
                                <option value="24">24</option>
                                <option value="26">26</option>
                                <option value="28">28</option>
                                <option value="30">30</option>
                                <option value="32">32</option>
                                <option value="34">34</option>
                                <option value="36">36</option>
                                <option value="38">38</option>
                                <option value="40">40</option> 
                            </select>
                            <span class="aged-third">Years</span>
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-select-search">
                        <div class="default-select"> 
                            <select name="religion">
                                @foreach($religions as $religion)
                                <option value="{{ $religion->id }}">{{ $religion->english_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-select-search">
                        <div class="default-select" id="default-selects"> 
                            <select name="division">
                                @foreach($divisions as $division)
                                <option value="{{ $division->id }}">{{ $division->english_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 form-cols home-select-search">
                        <button type="submit" name="submit" class="btn btn-info">Search</button>
                    </div>
                </div>
            </form>                
<!--            <p class="text-white">Your <span>Love</span> story is waiting to happen!</p>-->

            </div>
        </div>
    </div>
</section>
<!--Home Page Banner Area-->

<section class="features-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="single-feature">
                <h4>{{trans('welcome.feature_1')}}</h4>
                    <p>Register for free & put up your Profile</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="single-feature">
                    <h4>{{trans('welcome.feature_2')}}</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="single-feature">
                    <h4>{{trans('welcome.feature_3')}}</h4>
                    <p>Select & Connect with Matches you like</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="single-feature">
                    <h4>{{trans('welcome.feature_4')}}</h4>
                    <p>Become a Premium Member & Start a Conversation</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="popular-post-area pt-100">
    <div class="container">
       
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-10">
                <div class="title text-center">
                <h1 class="mb-10">{{trans('welcome.slider_title')}}</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut dolore magna aliqua.</p>
                </div>
            </div>
        </div>
       
        <div class="row align-items-center">
            <div class="active-popular-post-carusel">
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img class="img-fluid" src="{{url('assets/img/pages/t1.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Onik</h4></a>
<!--                        <h6>Sylhet</h6>-->
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img src="{{url('assets/img/pages/t2.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Likhon</h4></a>
<!--                        <h6>Mumensingh</h6>-->
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img src="{{url('assets/img/pages/t3.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Mamun</h4></a>
<!--                        <h6>Pabna</h6>-->
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img src="{{url('assets/img/pages/t4.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Lia</h4></a>
<!--                        <h6>Dhaka</h6>-->
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img src="{{url('assets/img/pages/t1.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Ratul</h4></a>
<!--                        <h6>Rongpur</h6>-->
                         <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <a href="#"><img src="{{url('assets/img/pages/t2.jpg')}}" alt=""></a>
                        <a class="btns text-uppercase" href="#">view more</a>
                    </div>
                    <div class="details">
                        <a href="#"><h4>Riad</h4></a>
<!--                        <h6>Jamalpur</h6>-->
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut labore et dolore magna aliqua ed do eiusmod temporinc ......
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="feature-cat-area" id="category">
    <div class="container">
        <div class="section-top-border">
            <h3 class="mb-30">About Marriage Matrimony</h3>
            <div class="row">
                <div class="col-lg-12">
                    <blockquote class="generic-blockquote">
                    “Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking”
                    </blockquote>
                </div>
            </div>
             
        </div>
    </div>
</section>

@endsection