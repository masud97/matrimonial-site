@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="offset-md-2 col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create Profile') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('user.profile.create3') }}" enctype="multipart/form-data">
                            @csrf
                            
                            

                            <div class="form-group row">
                                <label for="id-proof" class="col-md-4 col-form-label text-md-right">{{ __('Id Proof') }}</label>

                                <div class="col-md-6">
                                    <input id="id-proof" type="file" class="form-control @error('id-proof') is-invalid @enderror" name="id-proof" value="{{ old('id-proof') }}" autocomplete="id-proof">

                                    @error('id-proof')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="profile-image" class="col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

                                <div class="col-md-6">
                                    <input id="profile-image" type="file" class="form-control @error('profile-image') is-invalid @enderror" name="profile-image" value="{{ old('profile-image') }}" autocomplete="profile-image">

                                    @error('profile-image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="note" class="col-md-4 col-form-label text-md-right">{{ __('Note') }}</label>

                                <div class="col-md-6">                                
                                <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" name="note" value="{{ old('note') }}" autocomplete="note">
                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="verification" class="col-md-4 col-form-label text-md-right">{{ __('Verification Code') }}</label>

                                <div class="col-md-6">                                
                                <input id="verification" type="text" class="form-control @error('verification') is-invalid @enderror" name="verification" value="{{ old('verification') }}" autocomplete="verification">
                                    @error('verification')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>      

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
