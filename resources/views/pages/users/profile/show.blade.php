@extends('layouts.user')
@section('title', 'Profile')
@section('sliderText', 'Profile')
@section('content')
<div class="container">          
    <div class="section-top-border">
            <!--Start Of Nav Pills-->            
            <div class="tab-content" id="myTabContent">
              <div>
                <!--Start of Wizard Area-->
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-xs-12 col-md-8 offset-md-2 up-fam-profile">
                      <div class="wizard card-like profile-card">
                        <ul class="nav nav-tabs tabs-marker tabs-dark bg-dark" id="myTab" role="tablist">
                          <li class="nav-item">
                             <a class="nav-link active" id="profile" data-toggle="tab" href="#profile-tab" role="tab" aria-controls="profile" aria-selected="true">Profile<span class="marker"></span></a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="education" data-toggle="tab" href="#education-tab" role="tab" aria-controls="education" aria-selected="false">Education<span class="marker"></span></a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="family" data-toggle="tab" href="#family-tab" role="tab" aria-controls="family" aria-selected="false">Family Info<span class="marker"></span></a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="imageUp" data-toggle="tab" href="#image-tab" role="tab" aria-controls="imageUp" aria-selected="false">Image<span class="marker"></span></a>
                          </li>
                        </ul>
                        <form action="#">
                          <div class="wizard-header">
                            <div class="row">
                              <div class="col-md-12 col-xs-12"> 
                              </div>
                            </div>
                          </div>
                          <div class="wizard-body">
                           
                            <div class="tab-pane fade show active step initial" id="profile-tab" role="tabpanel" aria-labelledby="profile-tab">                     
                              <div class="row">                       
                                <div class="col-lg-12 col-md-12 col-xs-12">                    
                                    <form action="#" class="from_registration">
                                      <div class="mt-10">
                                          <input type="text" name="first_name" placeholder="Full Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Full Name'" value="@if(!empty($profileDetails->full_name)){{$profileDetails->full_name}}@endif" class="single-input">
                                      </div>
                                      <div class="mt-10 input-group date"  data-date-format="dd.mm.yyyy">
                                            <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                                <input type="dob" name="dob" placeholder="Date of Birth" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder='Date of Birth'" value="@if(!empty($profileDetails->dob)){{changeDateFormatToView($profileDetails->dob)}}@endif"  class="single-input">
                                            </div>                                    
                                      </div>                                                                                   
                                      <div class="mt-10">
                                            <div class="form-select religion-select">
                                                <select>
                                                    <option value="{{ $profileDetails->religion->english_name}}"  class="single-input">{{ $profileDetails->religion->english_name}}   </option>
                                                
                                                
                                                </select>


                                                <!-- <input type="text" name="religion" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder='Date of Birth'" value="@if(!empty($profileDetails->religion->english_name)){{ $profileDetails->religion->english_name}}@endif"  class="single-input"> -->
                                            </div>
                                      </div>                                                                                  
                                      <div class="mt-10 religiontik" id="islam">
                                            <div class="form-select" id="religion-select">
                                                <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->caste->english_name)){{ $profileDetails->caste->english_name}} @endif" class="single-input">
                                            </div>
                                        </div>
                                        
                                        <div class="mt-10">
                                            <div class="form-select" id="default-select">
                                                <div class="mt-10 hindutik" id="hindu">
                                                    <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->marital_status->english_name)){{ $profileDetails->marital_status->english_name}} @endif" class="single-input">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-10">
                                            <div class="form-select" id="default-select">
                                                <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->height)){{ inches2feet($profileDetails->height) }} @endif" class="single-input">
                                            </div>
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->weight)){{ $profileDetails->weight. ' Kg' }} @endif" class="single-input">
                                        </div> 
                                        <div class="mt-10">
                                            <div class="form-select" id="default-select">
                                                <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->blood_group->english_name)){{ $profileDetails->blood_group->english_name }} @endif" class="single-input">
                                            </div>
                                        </div>                   
                                        <div class="mt-10">
                                            <div class="form-select">
                                                <input type="text" name="Cast" placeholder="Cast Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->country->english_name)){{$profileDetails->country->english_name }} @endif" class="single-input">
                                            </div>
                                        </div> 
                                        @if($profileDetails->country->id == 18)              
                                        <div class="mt-10 bd-district" id="bd">
                                            <div class="form-select">
                                                <input type="text" name="Cast" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" value="@if(!empty($profileDetails->present_district->english_name)){{$profileDetails->present_district->english_name}} @endif" class="single-input">
                                            </div>
                                        </div>
                                        <div class="mt-10 bd-adress-area" id="dis">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->present_address)){{$profileDetails->present_address}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="mt-10 ame-state">
                                            <div class="form-select">
                                                <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->residence_type)){{ ucfirst($profileDetails->residence_type)}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                            </div>
                                        </div>
                                        @else  
                                        <div class="mt-10 ame-state">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->nrb->state)){{ ucfirst($profileDetails->nrb->state)}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>

                                        <div class="mt-10 ame-state">
                                            <input type="text" name="residence-address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->nrb->immigration->id)){{ $profileDetails->nrb->immigration->id }} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>                 
                                        @endif
                                                                       
                                        <div class="mt-10"> 
                                            <div class="form-select">
                                                <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->home_district->english_name)){{$profileDetails->home_district->english_name}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">                                                
                                            </div>
                                        </div>
                                        <div class="mt-10 home-origin" id="diso" style="display:none">
                                            <input type="text" name="address" placeholder="Origin Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Address'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="update_info_menu">
                                            <a href="#" class="modal-ticker-btn">Submit</a>                    
                                        </div>                                                                                      
                                  </form>
                                </div>                        
                              </div>                      
                            </div>
                            
                            <div class="step tab-pane fade"  id="education-tab" role="tabpanel" aria-labelledby="Education tab">
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">                    
                                    <form action="#" class="">

                                      <div class="mt-10">
                                            <div class="form-select">
                                                <input type="text" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->education->english_name)){{$profileDetails->education->english_name}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                            </div>
                                       </div>
                                       <div class="mt-10 edu-detail" id="edur">
                                            <small class="text-counter"> less then <b>20 words</b></small>
                                            <input type="text" name="detail" placeholder="Details Text 50 Max" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Details Text 50 Max'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                       <div class="mt-10 edu-institute">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->institution)){{ ucfirst($profileDetails->institution)}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>                             
                                        <div class="mt-10">
                                            <div class="form-select">
                                                <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->employment->english_name)){{$profileDetails->employment->english_name}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                            </div>
                                        </div>
                                                                    
                                        <div class="mt-10">
                                            <div class="form-select">
                                                <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->occupation->english_name)){{$profileDetails->occupation->english_name}} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                            </div>
                                        </div>
                                        <div class="mt-10 other-occ-area" id="occ-other">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->income_range->en_range_from)) {{ $profileDetails->income_range->en_range_from }} - {{ $profileDetails->income_range->en_range_to }}@endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        
                                        <div class="mt-10">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->employment->employer)) {{ $profileDetails->employment->employer }} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->employment->employer_address)) {{ $profileDetails->employment->employer_address }} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" value="@if(!empty($profileDetails->employment->position)) {{ $profileDetails->employment->position }} @endif" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>                                                                                       <div class="update_info_menu">
                                            <a href="#" class="modal-ticker-btn">Submit</a>                    
                                        </div>        
                                  </form>
                                </div> 
                              </div>
                            </div>
                            
                            <div class="step tab-pane fade"  id="family-tab" role="tabpanel" aria-labelledby="Family tab">
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">                    
                                    <form action="#" class="">
                                       <div class="mt-10 edu-detail" id="">
                                            <input type="text" name="detail" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Father's Name'" required class="single-input" value="@if(!empty($profileDetails->father_name)) {{ $profileDetails->father_name }} @endif" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                       <div class="mt-10 edu-institute">
                                            <input type="text" name="detail" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Father's Occupation'" required class="single-input" value="@if(!empty($profileDetails->father_occupation)) {{ $profileDetails->father_occupation }} @endif" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="mt-10 other-emp-area" id="">
                                            <input type="text" name="name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Mother's Name'" required class="single-input" value="@if(!empty($profileDetails->mother_name)) {{ $profileDetails->mother_name }} @endif" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div> 
                                        <div class="mt-10 other-occ-area" id="occ-other">
                                            <input type="text" name="name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Mother's Occupation'" required class="single-input" value="@if(!empty($profileDetails->mother_occupation)) {{ $profileDetails->mother_occupation }} @endif" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        </div>
                                        <div class="single-element-widget mt-30">
                                            <h6 class="mb-30">Sibling</h6>
                                            <div class="switch-wrap d-flex justify-content-between">
                                                <p>No Sibling</p>
                                                <div class="primary-checkbox">
                                                    <input type="checkbox" id="confirm-checkbox">
                                                    <label for="confirm-checkbox"></label>
                                                </div>
                                            </div>
                                            <div class="switch-wrap d-flex justify-content-between">
                                                <p>Brother</p>
                                                <div class="primary-checkbox">
                                                    <input type="checkbox" id="default-checkbox">
                                                    <label for="default-checkbox"></label>
                                                </div>
                                            </div>
                                            <div class="brother-div"  style="display: none">
                                                <div class="mt-10">
                                                    <input type="text" name="name" placeholder="Brother's Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Brother's Name'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                                </div>
                                                <div class="mt-10">
                                                    <input type="text" name="name" placeholder="Brother's Occupation" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Brother's Occupation'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">                                                    
                                                </div>
                                                <button id="add" class="btn add-more button-yellow uppercase" type="button">+ Add another</button> 
                                                <button class="delete btn button-white uppercase">- Remove</button>
                                            </div>
                                            <div class="switch-wrap d-flex justify-content-between">
                                                <p>Sister</p>
                                                <div class="primary-checkbox">
                                                    <input type="checkbox" id="primary-checkbox">
                                                    <label for="primary-checkbox"></label>
                                                </div>                                                    
                                            </div>
                                            <div class="sister-div" style="display: none">
                                                <div class="mt-10">
                                                    <input type="text" name="name" placeholder="Sister's Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Sister's Name'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                                </div>
                                                <div class="mt-10">
                                                    <input type="text" name="name" placeholder="Sister's Occupation" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Sister's Occupation'" required class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                                </div>
                                                <button id="sis-add" class="btn add-more button-yellow uppercase" type="button">+ Add another</button> 
                                                <button class="sis-delete btn button-white uppercase">- Remove</button>
                                            </div>                                      
                                        </div>	
                                        <div class="update_info_menu">
                                            <a href="#" class="modal-ticker-btn">Submit</a>                    
                                        </div>         
                                  </form>
                                </div> 
                              </div>
                            </div>
                            
                            <div class="step tab-pane fade"  id="image-tab" role="tabpanel" aria-labelledby="Image tab">
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">                    
                                    <form action="#" class="">
                                        <div class="mt-10 image-proof">
                                            <span>Change Profile Photo: </span>
                                            <div class="yes">
                                                <span class="btn_upload">
                                                  <input type="file" id="imag2" title="" class="input-img"/>CHANGE PHOTO
                                                </span>
                                                <img id="ImgPreview2" src="" class="preview2" />
                                                <input type="button" id="removeImage2" value="x" class="btn-rmv2 ticker-btn" />
                                            </div>
                                            <p>Note: Max image size 60kb</p>
                                        </div> 
                                        <div class="mt-10">                                               
                                           <div id="drop-area">
                                              <form class="my-form">
                                                <input type="file" id="fileElem" multiple accept="image/*" onchange="handleFiles(this.files)">
                                                <label class="btn_upload" for="fileElem">SELECT IMAGES</label>
                                              </form>
                                              <progress id="progress-bar" max=100 value=0></progress>
                                              <div id="gallery" />
                                           </div>
                                        </div>                                                
                                        <div class="update_info_menu">
                                            <a href="#" class="modal-ticker-btn">Submit</a>                    
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                          
                          </div>
                          
                          
                        </form>
                      </div>
                    </div>
                  </div>
                </div>    
                <!--Start of Wizard Area-->   
              </div>
            </div>
            <!--End Of Nav Pills--> 
    </div>
</div>
@endsection