@extends('layouts.profile')

@section('content')
<div class="whole-wrap">
    <div class="container"> 
                 
        <!--Start of Wizard Area-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-md-8 offset-md-2">
              <div class="wizard card-like">
                <form action="{{url('/user/profile/create1')}}" method="POST" enctype="multipart/form-data">
                    @csrf  
                    <div class="wizard-header">
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <h3 class="text-center">Create Profile<br><small></small></h3>                        
                        <div class="steps text-center">
                          <div class="wizard-step active"><span>01</span></div>
                          <div class="wizard-step"><span>02</span></div>
                          <div class="wizard-step"><span>03</span></div>
                          <div class="wizard-step"><span>04</span></div>
                        </div>
                        <hr/>
                      </div>
                    </div>
                  </div>
                  
                  <div class="wizard-body"> 
                                     
                    <div class="step initial active">                     
                      <div class="row">                       
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="mt-10">
                                  <div class="primary-switch">
                                     <input type="checkbox" name="name-privilege" id="default-switch" checked>
                                     <label for="default-switch"></label>
                                  </div>
                                  <small class="text-preferences"><b>Set ( Private / Public ) </b></small>
                                  <input type="text" name="f_name" id="name" placeholder="Full Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Full Name'" class="single-input">
                              </div>
                              <div class="mt-10 input-group date"  data-date-format="dd.mm.yyyy">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="dob" id="dob" name="dob" placeholder="Date of Birth" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder='Date of Birth'" class="single-input">
                                </div>                                    
                              </div>                                                                                   
                              <div class="mt-10">
                                <div class="form-select religion-select">
                                    <select class="religion_select" name="religion" id="religion">
                                        <option disabled selected>Religion</option>
                                        @foreach($religions as $religion)
                                            <option value = '{{ $religion->id }}'>{{ $religion->english_name }}</option>
                                        @endforeach
                                    </select>
                                 </div>
                              </div>                                                                                  
                              <div class="mt-10 religiontik religion-div" id="islam">
                                    <div class="form-select" id="religion-select">
                                        <select name="caste" id="caste" class="caste">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10 hindutik" id="hindu">
                                  <input type="text" name="caste-text" placeholder="Cast name if not found in dropdown" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Cast Name'" class="single-input">
                                </div>
                                
                                <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select id="marital_status" name="marital_status">
                                            <option value="">Marital Status</option>
                                            @foreach($maritalStatus as $status)
                                                <option value = '{{ $status->id }}'>{{ $status->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <!--
                                <div class="mt-10 has-child" id="have-children">
                                    <div class="form-select" id="default-select">
                                        <select id="has-child">
                                            <option value="1">Has Children</option>
                                            <option value="child-number">Yes</option>
                                            <option value="3">No</option>
                                        </select>
                                    </div>
                                </div>
                            -->
                                <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select name="height_in_feet" id="height_in_feet">
                                            <option value="">Height ft</option>
                                            <option value="4">4 ft</option>
                                            <option value="5">5 ft</option>
                                            <option value="6">6 ft</option>
                                            <option value="7">7 ft</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select name="height_in_inches" id="height_in_inches">
                                            <option value="">Height inch</option>
                                            <option value="1">1 inch</option>
                                            <option value="2">2 inch</option>
                                            <option value="3">3 inch</option>
                                            <option value="4">4 inch</option>
                                            <option value="5">5 inch</option>
                                            <option value="6">6 inch</option>
                                            <option value="7">7 inch</option>
                                            <option value="8">8 inch</option>
                                            <option value="9">9 inch</option>
                                            <option value="10">10 inch</option>
                                            <option value="11">11 inch</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10">
                                  <input type="text" id="weight" name="weight" placeholder="Weight" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Weight'" class="single-input">
                                </div> 
                                <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select id="blood_group" name="blood_group">
                                            <option value="">Blood Group</option>
                                            @foreach($bloodGroups as $bloodGroup)
                                                <option value = '{{ $bloodGroup->id }}'>{{ $bloodGroup->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                   
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="country-bd-select country-ame-select" name="nrb-country" id="country">
                                            <option selected disabled>Live in</option>
                                            @foreach($countries as $country)
                                            <option value = '{{ $country->id }}'>{{ $country->english_name }}</option>
                                            @endforeach    
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="mt-10 bd-adress-area" id="dis">
                                    <div class="primary-switch">
                                        <input type="checkbox" name="name-privilege" id="default-switch" checked>
                                        <label for="default-switch"></label>
                                    </div>
                                    <small class="text-preferences"><b>Set ( Private / Public ) </b></small>
                                    <input type="text" name="residence-address" id="present-address-text" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>
                                
                                <div class="mt-10 bd-adress-area bd-district">
                                    <div class="form-select">
                                        <select name="residency-type" id="residency-type">
                                            <option value="">Residency Type</option>
                                            <option value="rented">Rented</option>   
                                            <option value="owned">Owned</option>                                     
                                        </select>
                                    </div>
                                </div>

                                <div class="mt-10 bd-district" id="bd">
                                    <div class="form-select">
                                        <select class="dis_select" name="present_district" id="present-district">
                                            <option>Present District</option>
                                            @foreach($districts as $district)
                                            <option value = '{{ $district->id }}'>{{ $district->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                                
                                
                                <div class="mt-10 ame-state" style="display: none;">
                                    <input type="text" id="state" name="state" placeholder="State/Zip" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'State/Zip'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div> 
                                     
                                <div class="mt-10 ame-state" style="display: none;">
                                    <input type="text" name="residence-address" placeholder="Residence Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Residence Address'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div> 
                                
                                <div class="mt-10 ame-state" style="display:none">
                                    <div class="form-select">
                                        <select class="select-resident" name="immigration-type" id="immigration-type">
                                            <option selected disabled>Immigration type</option>
                                            @foreach($immigrations as $immigration)
                                            <option value = '{{ $immigration->id }}'>{{ $immigration->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="origin-select" name="home-district" id="home-district">
                                            <option>Origin From</option>
                                            @foreach($districts as $district)
                                            <option value = '{{ $district->id }}'>{{ $district->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10 home-origin" id="diso">
                                    <input type="text" name="address" placeholder="Origin Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Address'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div> 
                        </div>                        
                      </div>                      
                    </div>
                    
                    <div class="step">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">   
                              <div class="mt-10">
                                    <div class="form-select">
                                        <select class="education-level" name="education" id="education">
                                            <option value="">Education Level</option>
                                            @foreach($educations as $education)
                                            <option value="{{ $education->id }}">{{ $education->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                               </div>
                               <div class="mt-10 edu-detail" id="edur">
                                    <small class="text-counter"> less then <b>20 words</b></small>
                                    <input type="text" name="edu_detail" placeholder="Details Text 50 Max" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Details Text 50 Max'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>
                               <div class="mt-10 edu-institute">
                                    <input type="text" name="institute" id="institute" placeholder="Institution Name" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Institution Name'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>                             
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="emp-in" name="employmenttype" id="employmenttype">
                                            <option value="">Employment Type</option>
                                            @foreach($employeTypes as $employeType)
                                            <option value="{{ $employeType->id }}">{{ $employeType->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10 other-emp-area" id="emp-other">
                                    <input type="text" name="name" placeholder="Employed In" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'As a'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>                            
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="occ" name="occupation" id="occupation">
                                            <option value="">Occupation</option>
                                            @foreach($occupations as $occupation)
                                            <option value="{{ $occupation->id }}">{{ $occupation->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10 other-occ-area" id="occ-other">
                                    <input type="text" name="name" placeholder="Employed In" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Employed In'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select name="income" id="income">
                                            <option value="">Income Range</option>
                                            @foreach($imcomeRanges as $incomeRange)
                                                <option value="{{ $incomeRange->id }}">{{ $incomeRange->en_range_from }} - {{ $incomeRange->en_range_to }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <input type="text" name="employer" id="employer" placeholder="Employer" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Employer'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>
                                <div class="mt-10">
                                    <input type="text" name="emp-address" id="empaddress" placeholder="Employer Address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Employer Address'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div>
                                <div class="mt-10">
                                    <input type="text" name="position" id="position" placeholder="Position/Designation" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Position/Designation'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                </div> 
                        </div> 
                      </div>
                    </div>
                    
                    <div class="step">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">                                
                                <div class="mt-10 image-proof">
                                    <span>Upload ID Proof : </span>
                                    <div class="yes">
                                        <span class="btn_upload">
                                          <input type="file" id="imag" name="idproof" title="" class="input-img"/>ID PROOF
                                        </span>
                                        <img id="ImgPreview" src="" class="preview1" />
                                        <input type="button" id="removeImage1" value="x" class="btn-rmv1 ticker-btn" />
                                    </div>
                                    <p>Note: NiD/Passport/Birth Register/Driving Lisence or any other valid Photo ID</p>
                                </div>
                                <div class="mt-10 image-proof">
                                    <span>Upload Profile Photo : </span>
                                    <div class="yes">
                                        <span class="btn_upload">
                                            
                                          <input type="file" id="imag2" name="image" title="" class="input-img"/>PROFILE PHOTO
                                        </span>

                                        <img id="ImgPreview2" src="" class="preview2" />                                        
                                        <input type="button" id="removeImage2" value="x" class="btn-rmv2 ticker-btn" />
                                  </div>
                                  <p>Note: Max image size 60kb</p>
                                </div>
                                <div class="mt-10">
                                    <textarea id="note" name="personal-details" class="single-textarea" placeholder="Write something about you ...." onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Message'" data-cf-modified-c10bb454cc45607171f3df27-=""></textarea>
                                </div> 
                                <div class="mt-10">
                                    <div class="submit-message">
                                        <input type="text" id="vcode" name="code" placeholder="Validation Code..." onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Validation Code...'" class="single-input" data-cf-modified-c10bb454cc45607171f3df27-="">
                                        <div class="thumb">
                                            <a class="validation-btn" href="" onclick="event.preventDefault(); resendVerificationCode();">Resend Code</a>
                                        </div>
                                    </div>
                                </div>                                   
                        </div> 
                      </div>
                    </div>
                    
                    <div class="step">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12"> 
                               <div class="mt-10">
                                    <div class="submit-message">Note: Submitted successful… will be published after verification send Email/Mobile notification</div>
                                </div> 
                        </div> 
                      </div>
                    </div>
                    
                  </div>
                  
                  <div class="wizard-footer">
                    <div class="row">
                      <div class="col-xs-6 pull-left block-center">
                        <a  id="wizard-prev" class="step-btn" style="display:none">Back</a>           
                      </div>
                      <div class="col-xs-6 pull-right text-center">
                        <a  id="wizard-next" class="step-btn" style="display:none">Continue</a>
                      </div>
                      <div class="col-xs-6 pull-right block-center">
                        <button  type="submit" name="submit" id="wizard-subm"class="step-btn">Submit</button>
                      </div>
                    </div>
                  </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>    
        <!--Start of Wizard Area-->       
    </div>
</div>
@endsection
