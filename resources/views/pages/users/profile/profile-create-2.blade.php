@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="offset-md-2 col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.profile.create2') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="heigher-education" class="col-md-4 col-form-label text-md-right">{{ __('Heigher Education Label') }}</label>

                            <div class="col-md-6">
                                <select id="heigher-education" class="form-control @error('heigher-education') is-invalid @enderror" name="heigher-education">
                                    @foreach($educations as $education)
                                    <option value="{{ $education->id }}">{{ $education->english_name }}</option>
                                    @endforeach
                                </select>
                                @error('heigher-education')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="institution" class="col-md-4 col-form-label text-md-right">{{ __('Institution') }}</label>

                            <div class="col-md-6">
                                <input id="institution" type="text" class="form-control @error('institution') is-invalid @enderror" name="institution" value="{{ old('institution') }}" autocomplete="institution">

                                @error('institution')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="educational-details" class="col-md-4 col-form-label text-md-right">{{ __('Short Description') }}</label>

                            <div class="col-md-6">
                                <input id="educational-details" type="text" class="form-control @error('educational-details') is-invalid @enderror" name="educational-details" value="{{ old('educational-details') }}" autocomplete="educational-details">

                                @error('educational-details')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="service" class="col-md-4 col-form-label text-md-right">{{ __('Service') }}</label>

                            <div class="col-md-6">                                
                                <select id="service" class="form-control @error('service') is-invalid @enderror" name="service">
                                @foreach($employeTypes as $employeType)
                                <option value="{{ $employeType->id }}">{{ $employeType->english_name }}</option>
                                @endforeach
                                </select>
                                @error('service')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="occupation" class="col-md-4 col-form-label text-md-right">{{ __('Occupation') }}</label>

                            <div class="col-md-6">                                
                                <select id="occupation" class="form-control @error('occupation') is-invalid @enderror" name="occupation">
                                @foreach($occupations as $occupation)
                                <option value="{{ $occupation->id }}">{{ $occupation->english_name }}</option>
                                @endforeach
                                </select>
                                @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="salary-range" class="col-md-4 col-form-label text-md-right">{{ __('Salary Range') }}</label>

                            <div class="col-md-6">                                
                                <select id="salary-range" class="form-control @error('salary-range') is-invalid @enderror" name="salary-range">
                                    @foreach($imcomeRanges as $incomeRange)
                                        <option value="{{ $incomeRange->id }}">{{ $incomeRange->en_range_from }} - {{ $incomeRange->en_range_to }}</option>
                                    @endforeach
                                </select>
                                @error('salary-range')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="employer" class="col-md-4 col-form-label text-md-right">{{ __('Employer') }}</label>

                            <div class="col-md-6">
                                <input id="employer" type="text" class="form-control @error('employer') is-invalid @enderror" name="employer" value="{{ old('employer') }}" autocomplete="employer">

                                @error('employer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="employer-address" class="col-md-4 col-form-label text-md-right">{{ __('Employer Address') }}</label>

                            <div class="col-md-6">
                                <input id="employer-address" type="text" class="form-control @error('employer-address') is-invalid @enderror" name="employer-address" value="{{ old('employer-address') }}" autocomplete="employer-address">

                                @error('employer-address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="position" class="col-md-4 col-form-label text-md-right">{{ __('position') }}</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ old('position') }}" autocomplete="position">

                                @error('position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>            

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save & Continue') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
