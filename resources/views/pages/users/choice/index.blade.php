@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Set Preference') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.choice.store') }}">
                        @csrf                        
                        <div class="form-group row">
                            <label for="height" class="col-md-4 col-form-label text-md-right">{{ __('Age') }}</label>

                            <div class="col-md-6"> 
                                <div class="form-control">                               
                                    <select id="age-from" class=" @error('age-from') is-invalid @enderror" name="age-from">
                                        <option value="">From</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>                                        
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="23">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>                                        
                                    </select>
                                    <select id="age-to" class=" @error('age-to') is-invalid @enderror" name="age-to">
                                        <option value="">To</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>
                                        <option value="36">36</option>
                                        <option value="37">37</option>
                                        <option value="38">38</option>
                                        <option value="39">39</option>
                                        <option value="40">40</option>
                                        <option value="41">41</option>
                                        <option value="42">42</option>
                                        <option value="43">43</option>
                                        <option value="44">44</option>
                                        <option value="45">45</option>
                                        <option value="46">46</option>
                                        <option value="47">47</option>
                                    </select>
                                </div>
                                @error('height_in_feet')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="feet-from" class="col-md-4 col-form-label text-md-right">{{ __('Height From') }}</label>
    
                                <div class="col-md-6"> 
                                    <div class="form-control">                               
                                        <select id="feet-from" class=" @error('feet-from') is-invalid @enderror" name="feet-from">
                                            <option value="">Feet</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                        <select id="inche-from" class=" @error('inche-from') is-invalid @enderror" name="inche-from">
                                            <option value="">Inches</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    @error('inche-from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            
                            <div class="form-group row">
                                    <label for="feet-to" class="col-md-4 col-form-label text-md-right">{{ __('Height To') }}</label>
        
                                    <div class="col-md-6"> 
                                        <div class="form-control">                               
                                            <select id="feet-to" class=" @error('feet-to') is-invalid @enderror" name="feet-to">
                                                <option value="">Feet</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                            </select>
                                            <select id="inche-to" class=" @error('inche-to') is-invalid @enderror" name="inche-to">
                                                <option value="">Inches</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        @error('inche-to')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div> 
                        
                        <div class="form-group row">
                            <label for="marital-status" class="col-md-4 col-form-label text-md-right">{{ __('Marital Status') }}</label>

                            <div class="col-md-6">                                
                                <select id="marital-status" class="form-control @error('marital-status') is-invalid @enderror" name="marital-status">
                                    @foreach($maritalStatus as $status)
                                        <option value="{{ $status->id }}">{{ $status->english_name }}</option>
                                    @endforeach                                    
                                </select>
                                @error('religion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="division" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>

                            <div class="col-md-6">                                
                                <select id="division" class="form-control @error('division') is-invalid @enderror" name="division">
                                    @foreach($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->english_name }}</option>
                                    @endforeach
                                </select>
                                @error('division')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="education" class="col-md-4 col-form-label text-md-right">{{ __('Education') }}</label>

                            <div class="col-md-6">                                
                                <select id="education" class="form-control @error('education') is-invalid @enderror" name="education">
                                    @foreach($educations as $education)
                                        <option value="{{ $education->id }}">{{ $education->english_name }}</option>
                                    @endforeach
                                </select>
                                @error('education')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="employeType" class="col-md-4 col-form-label text-md-right">{{ __('employe Type') }}</label>

                            <div class="col-md-6">                                
                                <select id="employe-type" class="form-control @error('employe-type') is-invalid @enderror" name="employe-type">
                                    @foreach($employeTypes as $employeType)
                                        <option value="{{ $employeType->id }}">{{ $employeType->english_name }}</option>
                                    @endforeach
                                </select>
                                @error('employeType')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="occupation" class="col-md-4 col-form-label text-md-right">{{ __('Occupation') }}</label>

                            <div class="col-md-6">                                
                                <select id="occupation" class="form-control @error('occupation') is-invalid @enderror" name="occupation">
                                    @foreach($occupations as $occupation)
                                        <option value="{{ $occupation->id }}">{{ $occupation->english_name }}</option>
                                    @endforeach
                                </select>
                                @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="income-range" class="col-md-4 col-form-label text-md-right">{{ __('Income Range') }}</label>

                            <div class="col-md-6">                                
                                <select id="income-range" class="form-control @error('income-range') is-invalid @enderror" name="income-range">
                                    @foreach($imcomeRanges as $imcomeRange)
                                        <option value="{{ $imcomeRange->id }}">{{ $imcomeRange->en_range_from }} - {{ $imcomeRange->en_range_to }}</option>
                                    @endforeach
                                </select>
                                @error('income-range')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save & Continue') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
