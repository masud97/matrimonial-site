@extends('layouts.user')
@section('title', 'Preferences')
@section('sliderText', 'Your Preferences')

@section('content')
<div class="whole-wrap">
    <div class="container"> 
                 
        <!--Start of Wizard Area-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-md-8 offset-md-2">
              <div class="wizard card-like">                
                  <div class="wizard-header">
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <h3 class="text-center">Set Your Preferences<br><small></small></h3>
                      </div>
                    </div>
                  </div>
                  
                  <div class="wizard-body">
                    <div class="step initial active">                     
                      <div class="row">                       
                        <div class="col-lg-12 col-md-12 col-xs-12">                    
                        <form action="{{route('user.choice.store')}}" method="POST" class="from_registration">    
                                @csrf                                                                              
                              <div class="mt-10">
                                <div class="form-select">
                                    <select class="religion_select" name="age-from">
                                        <option value="">From Age</option>
                                        <option value="18">18</option>
                                        <option value="20">20</option>
                                        <option value="22">22</option>
                                        <option value="24">24</option>
                                        <option value="26">26</option>
                                        <option value="28">28</option>
                                        <option value="30">30</option>
                                        <option value="32">32</option>
                                        <option value="34">34</option>
                                        <option value="36">36</option>
                                        <option value="38">38</option>
                                        <option value="40">40</option>
                                    </select>
                                 </div>
                              </div>              
                              <div class="mt-10">
                                <div class="form-select">
                                    <select class="religion_select" name="age-to">
                                        <option value="">To Age</option>                                        
                                        <option value="20">20</option>
                                        <option value="22">22</option>
                                        <option value="24">24</option>
                                        <option value="26">26</option>
                                        <option value="28">28</option>
                                        <option value="30">30</option>
                                        <option value="32">32</option>
                                        <option value="34">34</option>
                                        <option value="36">36</option>
                                        <option value="38">38</option>
                                        <option value="40">40</option>
                                        <option value="18">42</option>
                                    </select>
                                 </div>
                              </div> 
                              <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select name="feet-from">
                                            <option value="">From Height</option>                                            
                                            <option value="4">4 ft</option>
                                            <option value="5">5 ft</option>
                                            <option value="6">6 ft</option>
                                            <option value="7">7 ft</option>
                                            <option value="8">8 ft</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="form-select" id="default-select">
                                        <select name="feet-to">
                                            <option value="">To Height</option>                                            
                                            <option value="4">4 ft</option>
                                            <option value="5">5 ft</option>
                                            <option value="6">6 ft</option>
                                            <option value="7">7 ft</option>
                                            <option value="8">8 ft</option>
                                        </select>
                                    </div>
                              </div>                                                                                 
                              <div class="mt-10">
                                    <div class="form-select">
                                        <select name="marital-status">
                                            <option value="">Marital Status</option>
                                            @foreach($maritalStatus as $status)
                                                <option value="{{ $status->id }}">{{ $status->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="country-bd-select country-ame-select" name="division">
                                            <option value="">Division</option>
                                            @foreach($divisions as $division)
                                                <option value="{{ $division->id }}">{{ $division->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                    
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="dis_select" name="education">
                                            <option value="">Education</option>
                                            @foreach($educations as $education)
                                                <option value="{{ $education->id }}">{{ $education->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                  
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="ame-state-area" name="employe-type">
                                            <option value="">Employment</option>
                                            @foreach($employeTypes as $employeType)
                                                <option value="{{ $employeType->id }}">{{ $employeType->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                 
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="select-resident" name="occupation">
                                            <option value="">Occupation</option>
                                            @foreach($occupations as $occupation)
                                                <option value="{{ $occupation->id }}">{{ $occupation->english_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                                                             
                                <div class="mt-10">
                                    <div class="form-select">
                                        <select class="origin-select" name="income-range">
                                            <option value="">Income Range</option>
                                            @foreach($imcomeRanges as $imcomeRange)
                                                <option value="{{ $imcomeRange->id }}">{{ $imcomeRange->en_range_from }} - {{ $imcomeRange->en_range_to }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                            
                        </div>                        
                      </div>                      
                    </div>   
                  </div>
                  
                  <div class="wizard-footer">
                    <div class="row">                      
                      <div class="col-xs-6 pull-right text-center">
                        <button type="submit" name="submit" class="step-btn modal-ticker-btn">Save</button>
                      </div>
                    </div>
                  </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>    
        <!--Start of Wizard Area-->       
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    //hide show
    $(function () {
        //Marital status
        $('#marital_status').change(function () {
            var select=$(this).find(':selected').val();        
            $(".has-child").hide("slow");
            $('#' + select).show("slow");
        }).change();
        //Marital status
        
        //Religion select
        $('.religion_select').change(function () {
            var select=$(this).find(':selected').val();        
            $(".religiontik").hide("slow");
            $(".hindutik").hide("slow");
            $('#' + select).show("slow");
        }).change();
        //Religion select
        
        //country select
        $('.country-bd-select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".bd-district").hide("slow");
            $(".bd-adress-area").hide("slow");
            $('#' + select).show("slow");
        }).change();
        $('.dis_select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".bd-adress-area").hide("slow");
            $('#' + select).show("slow");
        }).change(); 
        
        $('.country-ame-select').on('change', function() {
          $('.ame-state').css('display', 'none');
          if ( $(this).val() === 'ame' ) {
            $('.ame-state').slideToggle().css('display', 'block');
          }
        });
    
        $('.origin-select').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".home-origin").hide("slow");
            $('#' + select).show("slow");
        }).change(); 
    
        $('.education-level').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".edu-detail").hide("slow");
            $('#' + select).show("slow");
        }).change();  
    
        $('.emp-in').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".other-emp-area").hide("slow");
            $('#' + select).show("slow");
        }).change();  
    
        $('.occ').change(function () {
            var select=$(this).find(':selected').val(); 
            $(".other-occ-area").hide("slow");
            $('#' + select).show("slow");
        }).change(); 
        //country select 
        
    });
    //hide show        
    
    //Wizard area Js
    $(document).ready(function () {
    // Checking button status ( wether or not next/previous and
    // submit should be displayed )
    const checkButtons = (activeStep, stepsCount) => {
      const prevBtn = $("#wizard-prev");
      const nextBtn = $("#wizard-next");
      const submBtn = $("#wizard-subm");

      switch (activeStep / stepsCount) {
        case 0: // First Step
          prevBtn.hide();
          submBtn.hide();
          nextBtn.show();
          break;
        case 1: // Last Step
          nextBtn.hide();
          prevBtn.show();
          submBtn.show();
          break;
        default:
          submBtn.hide();
          prevBtn.show();
          nextBtn.show();
      }
    };
    // Scrolling the form to the middle of the screen if the form
    // is taller than the viewHeight
    const scrollWindow = (activeStepHeight, viewHeight) => {
      if (viewHeight < activeStepHeight) {
        $(window).scrollTop($(steps[activeStep]).offset().top - viewHeight / 2);
      }
    };
    // Setting the wizard body height, this is needed because
    // the steps inside of the body have position: absolute
    const setWizardHeight = activeStepHeight => {
      $(".wizard-body").height(activeStepHeight);
    };
    $(function() {
      // Form step counter (little cirecles at the top of the form)
      const wizardSteps = $(".wizard-header .wizard-step");
      // Form steps (actual steps)
      const steps = $(".wizard-body .step");
      // Number of steps (counting from 0)
      const stepsCount = steps.length - 1;
      // Screen Height
      const viewHeight = $(window).height();
      // Current step being shown (counting from 0)
      let activeStep = 0;
      // Height of the current step
      let activeStepHeight = $(steps[activeStep]).height();
      checkButtons(activeStep, stepsCount);
      setWizardHeight(activeStepHeight);
      // Resizing wizard body when the viewport changes
      $(window).resize(function() {
        setWizardHeight($(steps[activeStep]).height());
      });
      // Previous button handler
      $("#wizard-prev").click(() => {
        // Sliding out current step
        $(steps[activeStep]).removeClass("active");
        $(wizardSteps[activeStep]).removeClass("active");
        activeStep--;
        // Sliding in previous Step
        $(steps[activeStep]).removeClass("off").addClass("active");
        $(wizardSteps[activeStep]).addClass("active");

        activeStepHeight = $(steps[activeStep]).height();
        setWizardHeight(activeStepHeight);
        checkButtons(activeStep, stepsCount);
      });
      // Next button handler
      $("#wizard-next").click(() => {
        // Sliding out current step
        $(steps[activeStep]).removeClass("inital").addClass("off").removeClass("active");
        $(wizardSteps[activeStep]).removeClass("active");
        // Next step
        activeStep++;
        // Sliding in next step
        $(steps[activeStep]).addClass("active");
        $(wizardSteps[activeStep]).addClass("active");

        activeStepHeight = $(steps[activeStep]).height();
        setWizardHeight(activeStepHeight);
        checkButtons(activeStep, stepsCount);
      });
    });    
    });

    //Wizard area Js    
</script>
@endpush