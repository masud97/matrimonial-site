@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">                
                @foreach($selectedCandidates as $candidate)
                <div class="panel-body">
                    <p><img src="{{asset('storage/candidates/profiles/'.$candidate->image)}}" height="180px" width="160px"></p>
                    <p>Name: {{ $candidate->name_status == 0 ? $candidate->full_name : 'Private' }}</p>
                    <p>Age: {{ dob2age($candidate->dob) }}</p>
                    <p>Height: {{ inches2feet($candidate->height) }}</p>
                    <p>Religion: {{ $candidate->religion->english_name }}</p>
                    <p>Marital Status: {{ $candidate->marital_status->english_name }}</p>
                    <p>Employe type: {{ $candidate->employe_type->english_name }}</p>
                    <p>Home District: {{ $candidate->home_district->english_name }}</p>
                    <p>Present District: {{ $candidate->present_district->english_name }}</p>
                    <p><a href="{{route('user.candidate.details', $candidate->id)}}">View Details</a></p>
                    <hr/>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
