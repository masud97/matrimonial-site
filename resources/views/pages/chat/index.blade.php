@extends('layouts.user')
@section('title', 'Chat')
@section('sliderText')
{{trans('dashboard.chat')}}
@endsection

@section('content')
<div class="whole-wrap">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-12 col-md-12"> 
             
                 <div class="people-list" id="people-list">
                      <div class="search">
                         <input type="text" placeholder="search" />
                      </div>              
                      <ul class="list"> 
                        @foreach($users as $key => $validUser) 
                            @if($validUser->sender_user_id == Auth::id())          
                                <li class="clearfix">
                                    <a href="" onclick="event.preventDefault(); viewMessages({{ $validUser->receiver_user->id }}); viewName({{ $validUser->receiver_user->id }}); viewName({{ $validUser->receiver_user->id }}); ">
                                        <img src="{{ url('assets/img/profile/'.$validUser->receiver_user->profile->image) }}" alt="avatar" />
                                        <div class="about">
                                            <div class="name">{{ $validUser->receiver_user->profile->full_name }}</div>
                                            <div class="status">
                                            @if($validUser->receiver_user->login_status == 1)
                                                <i class="fa fa-circle online"></i> online
                                            @else
                                                <i class="fa fa-circle offline"></i> offline
                                            @endif
                                            </div>
                                            <div class="chat-header"> 
                                            <small class="chat-message-note"><label id="{{ 'new_message_'.$key }}" style="float:left;"> 00 </label> <i class="fa fa-envelope"></i></small>
                                            </div>
                                        </div>
                                    </a>
                                </li> 
                            @else
                                <li class="clearfix">
                                    <a href="" onclick="event.preventDefault(); viewMessages({{ $validUser->sender_user->id }}); viewName({{ $validUser->sender_user->id }});">
                                        <img src="{{ url('assets/img/profile/'.$validUser->sender_user->profile->image) }}" alt="avatar" />
                                        <div class="about">
                                            <div class="name">{{ $validUser->sender_user->profile->full_name }}</div>
                                            <div class="status">
                                            @if($validUser->sender_user->login_status == 1)
                                                <i class="fa fa-circle online"></i> online
                                            @else
                                                <i class="fa fa-circle offline"></i> offline
                                            @endif
                                            </div>
                                            <div class="chat-header"> 
                                                <small class="chat-message-note"><label id="{{ 'new_message_'.$key }}" style="float:left;"> 00 </label> <i class="fa fa-envelope"></i></small>
                                            </div>
                                        </div>
                                    </a>
                                </li> 
                            @endif
                        @endforeach
                      </ul>                  
                 </div>
    
                 <div class="chat">
              <div class="chat-header clearfix">
                <div id="chat-with-image">
                  
                </div>
                <div class="chat-about">
                  <div class="chat-with" id="chat-with-name"></div>                  
                </div>                
              </div> <!-- end chat-header -->

              <!-- start chat-history -->
              <div class="full-chat-history">
              
              <div class="chat-history" id="scrolltoheight">
                <ul>
                  <div id="chat-message">
                    
                 </div>                  
                </ul>
              </div>              
              </div>
              <!-- end chat-history -->

              <div class="chat-message clearfix">
                <form id="message-from" class="message-from">
                  @csrf
                  <textarea name="message" id="message" placeholder ="Type your message" rows="3"></textarea>
                  <a href="#" class="genric-btn danger circle arrow" onclick="event.preventDefault(); sendNewMessage();">Send<span class="lnr lnr-arrow-right"></span></a>
                </form>
              </div> 
              <!-- end chat-message -->

            </div> <!-- end chat -->
    
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script type="38d45c5b43721184da46bd14-text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
  </script>
  <!--
  <script>
      var button = document.getElementById('hamburger-menu'),
      span = button.getElementsByTagName('span')[0];
      
      $('#hamburger-menu').on('click', toggleOnClass);
      function toggleOnClass(event) {
        var toggleElementId = '#' + $(this).data('toggle'),
        element = $(toggleElementId);
        element.toggleClass('on');
      }
      // close hamburger menu after click a
      $( '.menu li a' ).on("click", function(){
        $('#hamburger-menu').click();
      });
  </script>
-->
  <script type="text/javascript">
      
      //Start chat module javascript
      
      (function(){

var chat = {
  messageToSend: '',
  messageResponses: [
    'Why did the web developer leave the restaurant? Because of the table layout.',
    'How do you comfort a JavaScript bug? You console it.',
    'An SQL query enters a bar, approaches two tables and asks: "May I join you?"',
    'What is the most used language in programming? Profanity.',
    'What is the object-oriented way to become wealthy? Inheritance.',
    'An SEO expert walks into a bar, bars, pub, tavern, public house, Irish pub, drinks, beer, alcohol'
  ],
  init: function() {
    this.cacheDOM();
    this.bindEvents();
    this.render();
  },
  cacheDOM: function() {
    this.$chatHistory = $('.chat-history');
    this.$button = $('button');
    this.$textarea = $('#message-to-send');
    this.$chatHistoryList =  this.$chatHistory.find('ul');
  },
  bindEvents: function() {
    this.$button.on('click', this.addMessage.bind(this));
    this.$textarea.on('keyup', this.addMessageEnter.bind(this));
  },
  render: function() {
    this.scrollToBottom();
    if (this.messageToSend.trim() !== '') {
      var template = Handlebars.compile( $("#message-template").html());
      var context = { 
        messageOutput: this.messageToSend,
        time: this.getCurrentTime()
      };

      this.$chatHistoryList.append(template(context));
      this.scrollToBottom();
      this.$textarea.val('');
      
      // responses
      var templateResponse = Handlebars.compile( $("#message-response-template").html());
      var contextResponse = { 
        response: this.getRandomItem(this.messageResponses),
        time: this.getCurrentTime()
      };
      
      setTimeout(function() {
        this.$chatHistoryList.append(templateResponse(contextResponse));
        this.scrollToBottom();
      }.bind(this), 1500);
      
    }
    
  },
  
  addMessage: function() {
    this.messageToSend = this.$textarea.val()
    this.render();         
  },
  addMessageEnter: function(event) {
      // enter was pressed
      if (event.keyCode === 13) {
        this.addMessage();
      }
  },
  scrollToBottom: function() {
     this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
  },
  getCurrentTime: function() {
    return new Date().toLocaleTimeString().
            replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
  },
  getRandomItem: function(arr) {
    return arr[Math.floor(Math.random()*arr.length)];
  }
  
};

chat.init();

var searchFilter = {
  options: { valueNames: ['name'] },
  init: function() {
    var userList = new List('people-list', this.options);
    var noItems = $('<li id="no-items-found">No items found</li>');
    
    userList.on('updated', function(list) {
      if (list.matchingItems.length === 0) {
        $(list.list).append(noItems);
      } else {
        noItems.detach();
      }
    });
  }
};

searchFilter.init();

})();

      
      //End chat module javascript
      
      
      
      //hide show
      $(function () {
          //Marital status
          $('#marital_status').change(function () {
              var select=$(this).find(':selected').val();        
              $(".has-child").hide("slow");
              $('#' + select).show("slow");
          }).change();
          //Marital status
          
          //Religion select
          $('.religion_select').change(function () {
              var select=$(this).find(':selected').val();        
              $(".religiontik").hide("slow");
              $(".hindutik").hide("slow");
              $('#' + select).show("slow");
          }).change();
          //Religion select
          
          //country select
          $('.country-bd-select').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".bd-district").hide("slow");
              $(".bd-adress-area").hide("slow");
              $('#' + select).show("slow");
          }).change();
          $('.dis_select').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".bd-adress-area").hide("slow");
              $('#' + select).show("slow");
          }).change(); 
          
          $('.country-ame-select').on('change', function() {
            $('.ame-state').css('display', 'none');
            if ( $(this).val() === 'ame' ) {
              $('.ame-state').slideToggle().css('display', 'block');
            }
          });
      
          $('.origin-select').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".home-origin").hide("slow");
              $('#' + select).show("slow");
          }).change(); 
      
          $('.education-level').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".edu-detail").hide("slow");
              $('#' + select).show("slow");
          }).change();  
      
          $('.emp-in').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".other-emp-area").hide("slow");
              $('#' + select).show("slow");
          }).change();  
      
          $('.occ').change(function () {
              var select=$(this).find(':selected').val(); 
              $(".other-occ-area").hide("slow");
              $('#' + select).show("slow");
          }).change(); 
          //country select 
          
      });
      //hide show        
      
      //Wizard area Js
      $(document).ready(function () {
      // Checking button status ( wether or not next/previous and
      // submit should be displayed )
      const checkButtons = (activeStep, stepsCount) => {
        const prevBtn = $("#wizard-prev");
        const nextBtn = $("#wizard-next");
        const submBtn = $("#wizard-subm");

        switch (activeStep / stepsCount) {
          case 0: // First Step
            prevBtn.hide();
            submBtn.hide();
            nextBtn.show();
            break;
          case 1: // Last Step
            nextBtn.hide();
            prevBtn.show();
            submBtn.show();
            break;
          default:
            submBtn.hide();
            prevBtn.show();
            nextBtn.show();
        }
      };
      // Scrolling the form to the middle of the screen if the form
      // is taller than the viewHeight
      const scrollWindow = (activeStepHeight, viewHeight) => {
        if (viewHeight < activeStepHeight) {
          $(window).scrollTop($(steps[activeStep]).offset().top - viewHeight / 2);
        }
      };
      // Setting the wizard body height, this is needed because
      // the steps inside of the body have position: absolute
      const setWizardHeight = activeStepHeight => {
        $(".wizard-body").height(activeStepHeight);
      };
      $(function() {
        // Form step counter (little cirecles at the top of the form)
        const wizardSteps = $(".wizard-header .wizard-step");
        // Form steps (actual steps)
        const steps = $(".wizard-body .step");
        // Number of steps (counting from 0)
        const stepsCount = steps.length - 1;
        // Screen Height
        const viewHeight = $(window).height();
        // Current step being shown (counting from 0)
        let activeStep = 0;
        // Height of the current step
        let activeStepHeight = $(steps[activeStep]).height();
        checkButtons(activeStep, stepsCount);
        setWizardHeight(activeStepHeight);
        // Resizing wizard body when the viewport changes
        $(window).resize(function() {
          setWizardHeight($(steps[activeStep]).height());
        });
        // Previous button handler
        $("#wizard-prev").click(() => {
          // Sliding out current step
          $(steps[activeStep]).removeClass("active");
          $(wizardSteps[activeStep]).removeClass("active");
          activeStep--;
          // Sliding in previous Step
          $(steps[activeStep]).removeClass("off").addClass("active");
          $(wizardSteps[activeStep]).addClass("active");

          activeStepHeight = $(steps[activeStep]).height();
          setWizardHeight(activeStepHeight);
          checkButtons(activeStep, stepsCount);
        });
        // Next button handler
        $("#wizard-next").click(() => {
          // Sliding out current step
          $(steps[activeStep]).removeClass("inital").addClass("off").removeClass("active");
          $(wizardSteps[activeStep]).removeClass("active");
          // Next step
          activeStep++;
          // Sliding in next step
          $(steps[activeStep]).addClass("active");
          $(wizardSteps[activeStep]).addClass("active");

          activeStepHeight = $(steps[activeStep]).height();
          setWizardHeight(activeStepHeight);
          checkButtons(activeStep, stepsCount);
        });
      });    
      });

      //Wizard area Js

      
  </script>
  <script>
    var receiverId;
    function viewMessages(id){      
      var url = "{{url('user/message')}}/" + id;
      receiverId = id;
      $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {          
          var $messageDisplay = $('#chat-message');
          $messageDisplay.empty();
          for (var i = 0; i < data.messages.length; i++) {         
            var messageId = data.messages[i]['sender_user_id'];
            if(messageId == id){
              $messageDisplay.append('<li>'
                +'<div class="message-data">'
                    +'<span class="message-data-time" >'+data.messages[i]['created_at']+'</span>'                    
                +'</div>'                 
                +'<div class="message my-message">'+data.messages[i]['message']+'</div>'
              +'</li>'
              );
            }else{    
              $messageDisplay.append('<li class="clearfix">'
                +'<div class="message-data align-right">'
                    +'<span class="message-data-time" >'+data.messages[i]['created_at']+'</span>'                    
                +'</div>' 
                +'<a href="" class="mess_delete" id="btnHistory" onclick="event.preventDefault(); deleteMessage('+data.messages[i]["id"]+');"><i class="fa fa-times" aria-hidden="true"></i></a>'
                +'<div class="message other-message float-right">'+data.messages[i]['message']+'</div>'
              +'</li>'
              );         

            }
            
          }
        });
        
    }

    function deleteMessage(id){
      var url = "{{url('user/delete/message')}}/" + id;
      $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {        
         
        });
    }
    
    function viewName(id){
      var url = "{{url('user/view/name')}}/" + id;         
      $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {        
          var showImage = $('#chat-with-image');
          showImage.empty();
          showImage.append('<img src="{{url("assets/img/profile")}}/'+data.userName['image']+'" alt="Avater"/>');        

          var userNameView = $('#chat-with-name');
          userNameView.empty();
          userNameView.append(data.userName['full_name']);
        });
    }

    function sendNewMessage(){
      var url = "{{url('user/send/message')}}";
      var data = $('#message-from').serialize() + "&id=" + receiverId;      
      $.ajax({
            url: url,
            method: "POST",
            data: data,
        }).done(function (data) {        
          $("#message").val('');
          setInterval(viewMessages(receiverId),1000); 
          setInterval(viewName(receiverId),1000);
        });    
    }    
  </script>
  <script>
    setInterval(function(){    
      if (typeof receiverId !== 'undefined') {
        viewMessages(receiverId);
        viewName(receiverId);
      }
      var url = "{{url('user/unread/message')}}";
      $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {        
          
        }); 
    }, 1000);          
  </script>
@endpush