@extends('layouts.fontend')

@section('content')
<!--Home Page Banner Area-->
<section class="banner-area relative" id="home">
  <div class="overlay overlay-bg"></div>
  <div class="container">
      <div class="row fullscreen d-flex align-items-center justify-content-center">
          <div class="banner-content col-lg-12">
          

          </div>
      </div>
  </div>
</section>
<!--Home Page Banner Area-->
<section class="sample-text-area">
  <div class="container">
      <h3 class="text-heading">Get started by saying hello to some of your Matches</h3>
  </div>
  <div class="container">
    @if(!empty($visitorSearch) && $visitorSearch->count())
      <div class="row">
          @foreach($visitorSearch as $result)
          <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="single-fcat button-open-modal">
                  <div class="primary-checkbox">
                      <input type="checkbox" id="default-checkbox" checked="">
                      <label for="default-checkbox"></label>
                  </div>
                  <a href="#" onclick="event.preventDefault();" class="button-mo">
                      @if(!is_null($result->image))
                      <img src="{{url('assets/img/profile')}}/{{ $result->image }}" alt="">                    
                      @else
                      <img src="{{url('assets/img/profile/demo.jpg')}}" alt="">
                      @endif
                      <p>{{ $result->full_name }}</p>
                  <small>Age: {{dob2age($result->dob)}}</small>
                  </a>
              </div>
          </div>
          @endforeach
         
          <!-- Open Image Modal Area -->
          <div class="client_modal">
              <div class="single-fcat front">
                  <div class="primary-checkbox">
                      <input type="checkbox" id="default-checkbox" checked="">
                      <label for="default-checkbox"></label>
                  </div>
                  <a href="#popup1" class="button-mo">
                      @if(!is_null($result->image))
                      <img src="{{url('assets/img/profile')}}/{{ $result->image }}" alt="">                    
                      @else
                      <img src="{{url('assets/img/profile/demo.jpg')}}" alt="">
                      @endif                   
                      <p><div class="candidate-name"></div></p>
                      <small>Age: {{dob2age($result->dob)}}</small>
                  </a>
              </div>
              <div class="back">
                  <div class="content">                         
                      <div class="img-about">                         
                          <div class="profile-img-area">                            
                              <div class="profile_slider">
                                <a href="#" class="control_next"> > </a>
                                <a href="#" class="control_prev"> < </a>
                                <ul>
                                  <li class="profileimg"></li>
                                  <li><div class="album-1"></div></li>
                                  <li><div class="album-2"></div></li>
                                </ul>  
                              </div>
                          </div>
                          <div class="profile-about-text">
                              <h4><div class="candidate-name"></div></h4>
                              <small>Profile created by Self | Online Now</small> 
                              <p><div class="personal-details"></div></p>
                          </div>
                      </div>    
                      <table class="table table-borderless">
                          <thead>
                            <tr>
                              <th></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Age</td>
                              <td><div class="age"></div></td>
                            </tr>
                            <tr>
                              <td>Height</td>
                              <td><div class="height"></div></td>
                            </tr>

                            <tr>
                              <td>Religion</td>                                
                              <td><div class="religion"></div></td>  
                            </tr>
                            <tr>                               
                              <td>Caste</td>
                              <td><div class="caste"></div></td>
                            </tr>
                            <tr>  
                              <td>Location</td>
                              <td><div class="address"></div></td>
                            </tr>
                            <tr>  
                              <td>Education</td>
                              <td><div class="education"></div></td>
                            </tr>
                            <tr>  
                              <td>Profession</td>
                              <td><div class="profession"></div></td>
                            </tr>
                            <tr>  
                              <td class="update_info_menu">
                                 <a href="#" class="modal-ticker-btn">Connect</a>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
              <div class="opened">
                  <div class="content">                        
                      <div class="img-about">                         
                          <div class="profile-img-area">                            
                              <div class="profile_slider">
                                 <a href="#" class="control_next"> > </a>
                                 <a href="#" class="control_prev"> < </a>
                                 <ul>
                                  <li class="profileimg"></li>
                                  <li><div class="album-1"></div></li>
                                  <li><div class="album-2"></div></li>
                                 </ul>  
                              </div>
                          </div>
                          <div class="profile-about-text">
                              <h4><div class="candidate-name"></div></h4>
                              <small>Profile created by Self | Online Now</small>
                              <p><div class="personal-details"></div></p>
                          </div>
                      </div> 
                      <table class="table table-borderless">
                          <thead>
                            <tr>
                              <th></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>                              
                            <tr>
                              <td>Age</td>
                              <td><div class="age"></div></td>
                            </tr>
                            <tr>
                              <td>Height</td>
                              <td><div class="height"></div></td>
                            </tr>
                            <tr>
                              <td>Religion</td>                                
                              <td><div class="religion"></div></td>  
                            </tr>
                            <tr>                               
                              <td>Caste</td>
                              <td><div class="caste"></div></td>
                            </tr>
                            <tr>  
                              <td>Location</td>
                              <td><div class="address"></div></td>
                            </tr>
                            <tr>  
                              <td>Education</td>
                              <td><div class="education"></div></td>
                            </tr>
                            <tr>  
                              <td>Profession</td>
                              <td><div class="profession"></div></td>
                            </tr>                              
                            <tr>  
                              <td class="update_info_menu">
                                 <a href="#" class="modal-ticker-btn">Connect</a>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                  </div>
                  <div class="close">x</div>
              </div>
          </div>
          <div class="wrapper"></div>
          <!-- Open Image Modal Area -->    
      </div>
    @else
      <p>No matches found.</p>
    @endif
  </div>
</section>


@endsection