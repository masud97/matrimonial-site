@extends('layouts.demo')
@section('title', 'Demo page')
@section('sliderText', 'Demo Page')
@section('content')
<div class="container">
    <h3 class="text-heading">{{trans('dashboard.text_heading')}}</h3>
</div>
<div class="container">
   <div class="row justify-content-center d-flex">
       <div class="col-lg-6 col-md-6 col-sm-12 sidebar">
            <div class="">
                <div class="buttons">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <div class="desc">
                    <a href="{{route('user.view.profile')}}"><p>
                            <span>{{trans('dashboard.update_porfile')}}</span> <br> {{trans('dashboard.update_profile_sub')}}
                        </p></a>
                    </div>
                </div>
            </div>
       </div>
       <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="preference-open-modal">
                <div class="buttons">
                    <i class="fa fa-check-square" aria-hidden="true"></i>
                    <div class="desc">
                    <a href="{{route('user.choice.create')}}">
                           <p><span>{{trans('dashboard.set_preference')}}</span><br>{{trans('dashboard.set_preference_sub')}}</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>            
     </div>
    <br><br>
    <div class="row">
    @if($selectedCandidates)


    @if(!empty($candidate))
    <!-- Open Image Modal Area -->
    <div class="client_modal">
            <div class="single-fcat front">
                <div class="primary-checkbox">
                    <input type="checkbox" id="default-checkbox" checked="">
                    <label for="default-checkbox"></label>
                </div>
                <a href="#popup1" class="button-mo">
                    <img src="{{url('assets/img/profile/'.$candidate->image)}}" alt="Profile Photo">                    
                    <p><div class="name"></div></p>
                    <small>Age: <div class="age"></div>, <div class="height"></div>, <div class="district"></div></small>
                </a>
            </div>
            <div class="back">
                <div class="content">                         
                    <div class="img-about">                         
                        <div class="profile-img-area">                            
                            <div class="profile_slider">
                              <a href="#" class="control_next"> > </a>
                              <a href="#" class="control_prev"> < </a>
                              <ul>
                                <li><div class="profileimage"></div></li>
                                <li><div class="album1"></div></li>
                                <li><div class="album2"></div></li>
                              </ul>  
                            </div>
                        </div>
                        <div class="profile-about-text">
                            <h4><div class="name"></div></h4>
                            <small>Profile created by Self | Online Now</small> 
                            <p><div class="personal-details"></div></p>
                        </div>
                    </div>    
                    <table class="table table-borderless">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Age</td>
                            <td><div class="age"></div></td>
                          </tr>
                          <tr>
                            <td>Height</td>
                            <td><div class="height"></div></td>
                            </tr>
                          <tr>
                            <td>Religion</td>                                
                            <td><div class="religion"></div></td>  
                          </tr>
                          <tr>                               
                            <td>Community</td>
                            <td><div class="caste"></div></td>
                          </tr>
                          <tr>  
                            <td>Location</td>
                            <td><div class="district"></div></td>
                          </tr>
                          <tr>  
                            <td>Education</td>
                            <td><div class="education"></div></td>
                          </tr>
                          <tr>  
                            <td>Profession</td>
                            <td><div class="profession"></div></td>
                          </tr>
                          <tr>  
                            <td class="update_info_menu">
                              <div class="chat-request-btn"></div>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="opened">
                <div class="content">                        
                    <div class="img-about">                         
                        <div class="profile-img-area">                            
                            <div class="profile_slider">
                               <a href="#" class="control_next"> > </a>
                               <a href="#" class="control_prev"> < </a>
                               <ul>
                                 <li><div class="profileimage"></div></li>
                                 <li><div class="album1"></div></li>
                                 <li><div class="album2"></div></li>
                               </ul>  
                            </div>
                        </div>
                        <div class="profile-about-text">
                            <h4><div class="name"></div></h4>
                            <small>Profile created by Self | Online Now</small>
                            <p><div class="personal-details"></div></p>
                        </div>
                    </div> 
                    <table class="table table-borderless">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>                              
                          <tr>
                            <td>Age</td>
                            <td><div class="age"></div></td>
                          </tr>
                          <tr>
                            <td>Height</td>
                            <td><div class="height"></div></td>
                          </tr>
                          <tr>
                            <td>Religion</td>                                
                            <td><div class="religion"></div></td>  
                          </tr>
                          <tr>                               
                            <td>Community</td>
                            <td><div class="caste"></div></td>
                          </tr>
                          <tr>  
                            <td>Location</td>
                            <td><div class="district"></div></td>
                          </tr>
                          <tr>  
                            <td>Education</td>
                            <td><div class="education"></div></td>
                          </tr>
                          <tr>  
                            <td>Profession</td>
                            <td><div class="profession"></div></td>
                          </tr>                              
                          <tr>  
                            <td class="update_info_menu">
                               <div class="modal-ticker-btn"></div>
                            </td>

                            <td class="update_info_menu">
                              <div class="add-favourite-list"></div>
                           </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
                <div class="close">x</div>
            </div>
        </div>
        
        <div class="wrapper"></div>
        <!-- Open Image Modal Area -->
    @endif
    @foreach($selectedCandidates as $candidate)
      <div class="col-lg-3 col-md-3 col-sm-6">
        <div class="single-fcat button-open-modal">
            <div class="primary-checkbox">
                <input type="checkbox" id="default-checkbox" checked="">
                <label for="default-checkbox"></label>
            </div>
            <a href="#" onclick="event.preventDefault(); candidateDetails({{ $candidate->id }});" class="button-mo">
                <img src="{{url('assets/img/profile/'.$candidate->image)}}" alt="Profile Photo">                   
                <p>{{ $candidate->name_status == 0 ? $candidate->full_name : 'Private' }}</p>
                <small>Age: {{ dob2age($candidate->dob) }}, {{ inches2feet($candidate->height) }}, {{ $candidate->present_district->english_name }}</small>
            </a>
        </div>
      </div><!-- End column -->
    @endforeach  
          



      </div><!-- End column -->
    @endif

    </div><!-- End row --> 
</div>

@endsection

@push('js')
<script>
function candidateDetails(id){
  var url = "{{url('/candidate/details')}}/" + id;
  //console.log(url);
  $.ajax({
      url: url,
      method: "GET",
  }).done(function (data) {            
      $('#popup1').modal('show');
      // console.log(data.details);
      if(data.details.name_status == 0){
          var name = data.details.full_name;
      }else{
          var name = 'Private';
      }

      var today = new Date();
      var dob = new Date(data.details.dob);
      var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));


      var $nameField = $('.name');
      $nameField.empty();
      $nameField.append(name); 

      var $personalDetails = $('.personal-details');
      $personalDetails.empty();
      $personalDetails.append( data.details.personal_details );

      var $ageField = $('.age');
      $ageField.empty();
      $ageField.append(age);

      var $height = $('.height');
      $height.empty();
      $height.append( toFeet(data.details.height) );

      var $religion = $('.religion');
      $religion.empty();
      $religion.append( data.details.religion.english_name );

      var $caste = $('.caste');
      $caste.empty();
      $caste.append( data.details.caste.english_name );

      var $address = $('.district');
      $address.empty();
      $address.append( data.details.present_district.english_name );

      var $education = $('.education');
      $education.empty();
      $education.append( data.details.education.english_name );

      var $profession = $('.profession');
      $profession.empty();
      $profession.append( data.details.employe_type.english_name );            

      var $chatrequest = $('.chat-request-btn');
      $chatrequest.empty();
      $chatrequest.append( "<a href='#' onclick='event.preventDefault(); sendChatRequest("+data.details.id+");' class='modal-ticker-btn' id='chadrequest'>Chat Request</a>");

      var $favouriteList = $('.add-favourite-list');
      $favouriteList.empty();
      $favouriteList.append( "<a href='#' onclick='event.preventDefault(); addFavouriteList("+data.details.id+");' class='modal-ticker-btn' id='favouriteList'>Favourite</a>");

      var $profileimg = $('.profileimage');
      $profileimg.empty();
      $profileimg.append("<img src='{{url('assets/img/profile')}}/"+ data.details.image +"' />");

      var $album1 = $('.album1');
      $album1.empty();
      $album1.append("<img src='{{url('assets/img/album')}}/"+ data.details.album_1 +"' />");

      var $album2 = $('.album2');
      $album2.empty();
      $album2.append("<img src='{{url('assets/img/album')}}/"+ data.details.album_2 +"' />");

      
      $('#chatrequest').click(function() {
        console.log(data.details.id)
      });

      var $album2 = $('.album2');
      $album2.empty();
      $album2.append("<img src='{{url('assets/img/album')}}/"+ data.details.album_2 +"' />");

      });
}

function toFeet(n) {
var realFeet = ((n*0.393700) / 12);
var feet = Math.floor(realFeet);
var inches = Math.round((realFeet - feet) * 12);
return feet + " feet " + inches + ' inches';
}
</script>



@endpush