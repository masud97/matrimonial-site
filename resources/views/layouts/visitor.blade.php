<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        @include('pertials.visitor.head')
    </head>    
<body>

@include('pertials.visitor.header')


<section class="banner-area relative" id="home">
    @include('pertials.visitor.banner')
</section>

<div class="whole-wrap">
    @yield('content')
</div>


<footer class="footer-area section-gap">
    @include('pertials.visitor.footer')
</footer>
    @include('pertials.visitor.default-js')

</body>
</html>