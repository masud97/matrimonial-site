<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        @include('pertials.profile.head')
    </head>    
<body>

@include('pertials.profile.header')


<section class="banner-area relative" id="home">
    @include('pertials.profile.banner')
</section>

<div class="whole-wrap">
    @yield('content')
</div>


<footer class="footer-area section-gap">
    @include('pertials.profile.footer')
</footer>
    @include('pertials.profile.default-js')

</body>
</html>