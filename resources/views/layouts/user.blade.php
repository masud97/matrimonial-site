<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        @include('pertials.user.head')
    </head>
    
<body>

<header id="header" id="home">
    @include('pertials.user.header')
</header>



<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content banner-content col-lg-12">
                <h1 class="text-white">@yield('sliderText')</h1>
            </div>
        </div>
    </div>
</section>

<section class="sample-text-area">    
    @yield('content')
</section>


<footer class="footer-area section-gap">
    @include('pertials.user.footer')
</footer>

    @include('pertials.user.default-js')
    

</body>
</html>