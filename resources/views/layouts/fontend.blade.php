<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        @include('pertials.fontend.head')
    </head>

<body>

<header id="header" id="home">
    @include('pertials.fontend.header')
</header>

@yield('content')

<footer class="footer-area section-gap">
    @include('pertials.fontend.footer')
</footer>
@include('pertials.fontend.default-js')

</body>
</html>
