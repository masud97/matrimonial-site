<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Profile;
use App\NRB;
use App\Employment;
use App\Sibling;
use App\Choice;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        // $this->call(UsersTableSeeder::class);
        // User::truncate();
        // Profile::truncate();
        NRB::truncate();
         // Employment::truncate();
        // Sibling::truncate();
        //Choice::truncate();

        // $userQuantity = 700;
       // $profileQuantity = 700;
        $NRBQuantity = 150;
        // $employmentDetailsQuantity = 500;
        // $siblingQuantity = 800;
        // $choiceQuantity = 700;

        // factory(User::class, $userQuantity)->create();
        
        // factory(Profile::class, $profileQuantity)->create();
        factory(NRB::class, $NRBQuantity)->create();
         // factory(Employment::class, $employmentDetailsQuantity)->create();
        // factory(Sibling::class, $siblingQuantity)->create();
        // factory(Choice::class, $choiceQuantity)->create();
        
    }
}
