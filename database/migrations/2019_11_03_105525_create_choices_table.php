<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->integer('height_from')->nullable();
            $table->integer('height_to')->nullable();
            $table->unsignedBigInteger('marital_status_id')->index()->nullable();
            $table->unsignedBigInteger('division_id')->index()->nullable();
            $table->unsignedBigInteger('education_id')->index()->nullable();
            $table->unsignedBigInteger('employe_type_id')->index()->nullable();
            $table->unsignedBigInteger('occupation_id')->index()->nullable();    
            $table->unsignedBigInteger('income_range_id')->index()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('marital_status_id')->references('id')->on('marital_statuses')->onDelete('set null');
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('set null');
            $table->foreign('education_id')->references('id')->on('education')->onDelete('set null');
            $table->foreign('employe_type_id')->references('id')->on('employe_types')->onDelete('set null');
            $table->foreign('occupation_id')->references('id')->on('occupations')->onDelete('set null');
            $table->foreign('income_range_id')->references('id')->on('income_ranges')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('choices');
    }
}
