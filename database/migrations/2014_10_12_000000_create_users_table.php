<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('password');
            $table->string('image')->default('demo.jpg');            
            $table->unsignedBigInteger('for_whome_id')->index()->nullable();
            $table->string('gender',20)->nullable();              
            $table->rememberToken();
            $table->integer('profile_creation_stage')->default('1');
            $table->string('verification_token')->nullable();
            $table->string('resend_token_count')->default('3');
            $table->timestamp('token_generation_time')->nullable();
            $table->integer('verified')->default(User::UNVERIFIED_USER);
            $table->timestamps();
            $table->date('last_login')->nullable();
            $table->date('deleted_at')->nullable();
            $table->tinyInteger('status')->default('1');

            $table->foreign('for_whome_id')->references('id')->on('for_whomes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
