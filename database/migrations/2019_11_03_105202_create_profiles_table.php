<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unsigned()->index();
            $table->string('full_name');
            $table->boolean('name_status')->default('0');
            $table->date('dob')->nullable();
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->unsignedBigInteger('blood_group_id')->index()->nullable();
            $table->unsignedBigInteger('religion_id')->index()->nullable();
            $table->unsignedBigInteger('caste_id')->index()->nullable();
            $table->unsignedBigInteger('marital_status_id')->index()->nullable();
            $table->string('number_of_children',10)->nullable();
            $table->boolean('staying_with')->default(false);
            $table->string('present_address')->nullable();
            $table->boolean('pres_add_status')->default('0');     
            $table->unsignedBigInteger('present_district_id')->index()->nullable();
            $table->string('residence_type',20)->nullable();                   
            $table->string('permanent_address')->nullable();
            $table->boolean('perm_add_status')->default('0');
            $table->unsignedBigInteger('home_district_id')->index()->nullable();
            $table->unsignedBigInteger('division_id')->index()->nullable();
            $table->unsignedBigInteger('country_id')->index()->nullable();
            $table->unsignedBigInteger('education_id')->index()->nullable();
            $table->string('edu_details')->nullable();
            $table->string('institution')->nullable();
            $table->unsignedBigInteger('employe_type_id')->index()->nullable();
            $table->unsignedBigInteger('occupation_id')->index()->nullable();
            $table->string('father_name', 150)->nullable();
            $table->string('father_occupation', 150)->nullable();
            $table->string('mother_name', 150)->nullable();
            $table->string('mother_occupation', 150)->nullable();
            $table->text('personal_details')->nullable();
            $table->unsignedBigInteger('income_range_id')->index()->nullable();
            $table->string('id_proof')->nullable();
            $table->string('image')->nullable();
            $table->boolean('image_status')->default('0');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->date('deleted_at')->nullable();
            $table->tinyInteger('status')->default('1');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('blood_group_id')->references('id')->on('blood_groups')->onDelete('set null');
            $table->foreign('religion_id')->references('id')->on('religions')->onDelete('set null');
            $table->foreign('caste_id')->references('id')->on('castes')->onDelete('set null');
            $table->foreign('marital_status_id')->references('id')->on('marital_statuses')->onDelete('set null');
            $table->foreign('present_district_id')->references('id')->on('districts')->onDelete('set null');
            $table->foreign('home_district_id')->references('id')->on('districts')->onDelete('set null');
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->foreign('education_id')->references('id')->on('education')->onDelete('set null');
            $table->foreign('employe_type_id')->references('id')->on('employe_types')->onDelete('set null');
            $table->foreign('occupation_id')->references('id')->on('occupations')->onDelete('set null');
            $table->foreign('income_range_id')->references('id')->on('income_ranges')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
