<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Profile;
use App\District;
use App\Divison;
use App\Choice;
use App\Sibling;
use App\Employment;
use App\NRB;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => $faker->safeEmail,
        'mobile' => $faker->e164PhoneNumber,
        'password' =>'$2y$10$TRFqmSJYIrYSKHEooMbpd.BF7n1qp8DLFtSOHLobKYgJwu3QbS7ma',        
        'for_whome_id' => $faker->numberBetween(1,6),
        'gender' => $gender = $faker->randomElement(['male', 'female']),
        'image' => $gender == 'male' ? $faker->randomElement(['1.jpg', '2.jpg', '3.jpg', '4.jpg']) : $faker->randomElement(['5.jpg', '6.jpg', '7.jpg', '8.jpg']),
        'verification_token' => User::generateVerificationCode(),
        'verified' => User::UNVERIFIED_USER,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'last_login' => $faker->dateTime($max = 'now', $timezone = null),
        'remember_token' => str_random(10),
    ];
});


$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->numberBetween(1,700),
        'full_name' => $faker->name,
        'name_status' => $faker->numberBetween(0,1),
        'dob' => $faker->dateTime($max = '-18 years', $timezone = null),
        'height' => $faker->numberBetween(150,230),
        'weight' => $faker->numberBetween(40,100),
        'blood_group_id' => $faker->numberBetween(1,8),
        'religion_id' => $religion = $faker->numberBetween(1,2),
        'caste_id' => $religion == 1 ? $faker->numberBetween(1,2) : $faker->numberBetween(3,4),
        'marital_status_id' => $maritalStatus = $faker->numberBetween(1,5),
        'number_of_children' => $maritalStatus != 1 ? $faker->numberBetween(1,4) : 0,
        'staying_with' => $maritalStatus != 1 ? $faker->numberBetween(0,1) : 0,
        'present_address' => $faker->address,
        'pres_add_status' => $faker->numberBetween(0,1),
        'present_district_id' => $faker->numberBetween(1,64), 
        'residence_type' => $faker->randomElement(['owned', 'rented']),        
        'permanent_address' => $faker->address,
        'perm_add_status' => $faker->numberBetween(0,1),
        'home_district_id' => $district = $faker->numberBetween(1,64),
        'division_id' => District::divisionID($district),
        'country_id' => 18,        
        'education_id' => $faker->numberBetween(1,6),
        'edu_details' => $faker->paragraph(1),
        'institution' => $faker->company,
        'employe_type_id' => $employe = $faker->numberBetween(1, 10),
        'occupation_id' => $employe != 10 ? $faker->numberBetween(1, 3) : 0,        
        'father_name' => $faker->name,
        'father_occupation'=> $faker->jobTitle,
        'mother_name' => $faker->name,
        'mother_occupation' => $faker->jobTitle,
        'personal_details' => $faker->paragraph(2),
        'income_range_id' => $faker->numberBetween(1, 10),
        'id_proof' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg']), 
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg']),
        'image_status' => $faker->numberBetween(0,1),
        'is_approved' => $faker->randomElement([true, false]),
        'created_at' =>'2019-10-12'
    ];
});

$factory->define(NRB::class, function (Faker $faker) {
    return [
        'profile_id' =>  $faker->unique()->numberBetween(1,700),
        'country_id' => $faker->numberBetween(1,245),
        'immigration_type_id' => $faker->numberBetween(1,4),
        'created_at' =>'2017-12-12'
    ];
});


$factory->define(Employment::class, function (Faker $faker) {
    return [
        'profile_id' =>  $faker->unique()->numberBetween(1,700),
        'employer' => $faker->company,
        'employer_address' => $faker->streetAddress,
        'position' => $faker->jobTitle,
        'created_at' =>'2017-12-12'
    ];
});


$factory->define(Sibling::class, function (Faker $faker) {
    return [
        'profile_id' => User::all()->random()->id,
        'name' => $faker->name,
        'relation' => $faker->randomElement(['Uncle', 'Aunt', 'brother', 'sister', 'cousin', 'nephew']),
        'profession' => $faker->jobTitle,        
        'created_at' =>'2017-12-12'
    ];
});


$factory->define(Choice::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->numberBetween(1,700),
        'age_from' => $agefrom = $faker->numberBetween(18,35),
        'age_to' => $faker->numberBetween($agefrom,40),
        'height_from' => $heightfrom = $faker->numberBetween(150,230),
        'height_to' => $faker->numberBetween($heightfrom,230),
        'marital_status_id' => $maritalStatus = $faker->numberBetween(1,5),
        'division_id' => $faker->numberBetween(1,8),
        'education_id' => $faker->numberBetween(1,6),
        'employe_type_id' => $faker->numberBetween(1, 9),
        'occupation_id' => $faker->numberBetween(1, 3),
        'income_range_id' => $faker->numberBetween(1, 10),
        'created_at' =>'2017-12-12'
    ];
});
